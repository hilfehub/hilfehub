import { useState, useCallback } from "react";
import Axios from "axios";
import { useDebouncedCallback } from "use-debounce";
import { Addresses } from "../backend";

export type MapboxSearchType =
  | "poi"
  | "country"
  | "region"
  | "district"
  | "postcode"
  | "locality"
  | "place"
  | "neighboorhood"
  | "address";

interface MapboxSearchConfig {
  types: MapboxSearchType[];
  limit?: number;
}

export interface MapboxGeocodingFeature {
  id: string;
  type: "Feature";
  place_type: MapboxSearchType[];
  relevance: number;
  properties: {
    landmark?: boolean;
    address?: string;
    accuracy?: string;
    category?: string;
  };
  text: string;
  place_name: string;
  center: [number, number];
  geometry: {
    type: "Point";
    coordinates: [number, number];
  };
  address?: string;
  context: {
    id: string;
    wikidata?: string;
    text: string;
    short_code?: string;
  }[];
}

export interface MapboxGeocodingResult {
  type: "FeatureCollection";
  query: string[];
  features: MapboxGeocodingFeature[];
  attribution: string;
}

const getMapboxSearchUrl = (
  searchTerm: string,
  location: [number, number] | undefined,
  { limit, types }: MapboxSearchConfig
) => {
  let url = `https://api.mapbox.com/geocoding/v5/mapbox.places/${searchTerm}.json`;
  url += `?access_token=${process.env.MAPBOXGL_TOKEN}`;
  url += `&types=${types.join(",")}`;
  url += `&autocomplete=true`;

  if (limit) {
    url += `&limit=${limit}`;
  }

  if (location) {
    url += `&proximity=${location[0]},${location[1]}`;
  }

  return url;
};

export function useMapboxSearch(
  config: MapboxSearchConfig
): [
  MapboxGeocodingFeature[],
  (searchTerm: string, location?: [number, number]) => void,
  boolean
] {
  const [results, setResults] = useState<MapboxGeocodingFeature[]>([]);
  const [isLoading, setIsLoading] = useState(false);

  const updateSearchTerm = useCallback(
    async (searchTerm: string, location?: [number, number]) => {
      if (!searchTerm) {
        return;
      }

      setIsLoading(true);

      const {
        data: { features }
      } = await Axios.get<MapboxGeocodingResult>(
        getMapboxSearchUrl(searchTerm, location, config)
      );

      setIsLoading(false);
      setResults(features);
    },
    [setResults]
  );

  return [results, updateSearchTerm, isLoading];
}

export function useDebouncedMapboxSearch(
  config: MapboxSearchConfig,
  ms: number
): [
  MapboxGeocodingFeature[],
  (searchTerm: string, location?: [number, number]) => void,
  boolean
] {
  const [results, updateSearchTerm, isLoading] = useMapboxSearch(config);

  const [isWaitingForCall, setIsWaitingForCall] = useState(false);

  const [debouncedUpdate] = useDebouncedCallback(
    (searchTerm: string, location?: [number, number]) => {
      setIsWaitingForCall(false);
      updateSearchTerm(searchTerm, location);
    },
    ms
  );

  const callDebouncedAndSetIsLoading = useCallback(
    (searchTerm: string, location?: [number, number]) => {
      setIsWaitingForCall(true);
      debouncedUpdate(searchTerm, location);
    },
    [setIsWaitingForCall, debouncedUpdate]
  );

  return [results, callDebouncedAndSetIsLoading, isWaitingForCall || isLoading];
}

export function mapboxGeocodingFeatureToAddress(
  feature: MapboxGeocodingFeature
): Addresses.Create.Payload {
  const { text, place_name, place_type, id, center } = feature;

  if (place_type.includes("poi")) {
    const [
      name,
      streetAndNumber,
      town,
      districtAndZip,
      nation
    ] = place_name.split(", ");

    let zipCode: string;
    {
      const indexOfLastSpaceInDistrictAndZip = districtAndZip.lastIndexOf(" ");
      const district = districtAndZip.substring(
        0,
        indexOfLastSpaceInDistrictAndZip
      );
      zipCode = districtAndZip.substring(indexOfLastSpaceInDistrictAndZip + 1);
    }

    return {
      mapboxId: id,
      name: text,
      position: center,
      suffix: "",
      streetAndNumber,
      town,
      zipCode: zipCode
    };
  }

  return null as any;
}
