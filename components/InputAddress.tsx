import { Row, Col, Form, Input, Tooltip } from "antd";
import { QuestionCircleOutlined } from "@ant-design/icons";

interface InputProps {
  info?: string;
}

export default function InputAddress({ info = "" }: InputProps) {
  const input =
    info === "" ? (
      <Input placeholder="Straße und Hausnummer" />
    ) : (
      <Input
        placeholder="Straße und Hausnummer"
        suffix={
          <Tooltip title={info}>
            <QuestionCircleOutlined />
          </Tooltip>
        }
      />
    );

  return (
    <>
      <Row gutter={[8, 8]}>
        <Col>
          <Form.Item>{input}</Form.Item>
        </Col>
      </Row>
      <Row gutter={[8, 8]}>
        <Col span={12}>
          <Form.Item>
            <Input size="large" placeholder="PLZ" onChange={() => {}} />
          </Form.Item>
        </Col>
        <Col span={12}>
          <Form.Item>
            <Input placeholder="Stadt / Ortsteil" onChange={() => {}} />
          </Form.Item>
        </Col>
      </Row>
    </>
  );
}
