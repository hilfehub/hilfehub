import MenuLayout from "./MenuLayout";
import ActionButtonMenu from "./ActionButtonMenu";

export function LegalLayout(props: React.PropsWithChildren<{}>) {
  const { children } = props;

  return (
    <MenuLayout menu={<ActionButtonMenu type="invisible" />}>
      <div className="paddings-sides" style={{ margin: "0 auto" }}>
        {children}
      </div>
    </MenuLayout>
  );
}
