import { useToken } from "./TokenContext";
import axios, { AxiosRequestConfig, AxiosInstance, AxiosResponse } from "axios";
import useSWR from "swr";
import { useMemo } from "react";

export function useAxios() {
  const token = useToken();

  const instance = useMemo(
    () =>
      axios.create({
        baseURL: "/api",
        headers: {
          common: {
            Authorization: `Bearer ${token}`
          }
        }
      }),
    [token]
  );

  return instance;
}

export function useAuthenticatedSWR<Data = any, Error = any>(
  key:
    | AxiosRequestConfig
    | string
    | null
    | (() => AxiosRequestConfig | string | null)
) {
  const axios = useAxios();
  return useSWR<Data | undefined, Error>(
    () => {
      if (typeof key === "function") {
        const r = key();
        return !!r ? [r, axios] : null;
      }

      return !!key ? [key, axios] : null;
    },
    async (key: AxiosRequestConfig | string, axios: AxiosInstance) => {
      let r: AxiosResponse<Data>;

      function validateStatus(status: number) {
        if (status >= 200 && status < 300) {
          return true;
        }

        if (status === 404) {
          return true;
        }

        return false;
      }

      if (typeof key === "string") {
        r = await axios.request({ url: key, validateStatus });
      } else {
        r = await axios.request({ validateStatus, ...key }).then(r => r.data);
      }

      if (r.status === 404) {
        return undefined;
      }

      return r.data;
    }
  );
}
