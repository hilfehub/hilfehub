import {
  SetStateAction,
  ReactChild,
  useContext,
  useEffect,
  createContext,
  useState,
  PropsWithChildren,
  useCallback,
  useMemo
} from "react";
import { ButtonType } from "antd/lib/button";

interface ClickSubscriber {
  actionButtonClicked: () => void;
  backButtonClicked: () => void;
}

interface ButtonDefinition {
  caption?: ReactChild;
  disabled?: boolean;
  type?: ButtonType | "invisible";
}

type Unsubscribe = () => void;

export interface ActionButtonContextRef {
  clickAction: () => void;
  clickBack: () => void;
}

export interface ActionButtonContextType {
  actionButtonDefinition?: ButtonDefinition;
  setActionButton: React.Dispatch<SetStateAction<ButtonDefinition | undefined>>;
  backButtonDefinition?: ButtonDefinition;
  setBackButton: React.Dispatch<SetStateAction<ButtonDefinition | undefined>>;
  subscribe: (subscriber: ClickSubscriber) => Unsubscribe;
  subscribers: ClickSubscriber[];
  clickAction: () => void;
  clickBack: () => void;
}

const contextDefaults: ActionButtonContextType = {
  actionButtonDefinition: {
    caption: "Weiter",
    disabled: false,
    type: "primary"
  },
  setActionButton: () => {},
  setBackButton: () => {},
  subscribe: (subscriber: ClickSubscriber) => () => {},
  subscribers: [],
  clickAction: () => {},
  clickBack: () => {}
};

const ActionButtonContext = createContext<ActionButtonContextType>(
  contextDefaults
);

interface ActionButtonContextProviderProps {
  receiveRef?: React.RefCallback<ActionButtonContextRef>;
}

export function ActionButtonContextProvider(
  props: PropsWithChildren<ActionButtonContextProviderProps>
) {
  const { receiveRef, children } = props;

  const [definition, setDefinition] = useState<ButtonDefinition>();
  const [backButtonDefinition, setBackButton] = useState<ButtonDefinition>();
  const [subscribers, setSubscribers] = useState<ClickSubscriber[]>([]);

  const subscribe = useCallback(
    (subscriber: ClickSubscriber) => {
      setSubscribers(old => [...old, subscriber]);
      return () => {
        setSubscribers(old => old.filter(v => v !== subscriber));
      };
    },
    [setSubscribers]
  );

  const clickAction = useCallback(() => {
    subscribers.forEach(subscriber => {
      subscriber.actionButtonClicked();
    });
  }, [subscribers]);

  const clickBack = useCallback(() => {
    subscribers.forEach(subscriber => {
      subscriber.backButtonClicked();
    });
  }, [subscribers]);

  const memoContext = useMemo<ActionButtonContextType>(
    () => ({
      actionButtonDefinition: definition,
      setActionButton: setDefinition,
      subscribers: subscribers,
      backButtonDefinition,
      setBackButton,
      subscribe,
      clickAction,
      clickBack
    }),
    [
      definition,
      setDefinition,
      subscribers,
      subscribe,
      clickAction,
      clickBack,
      backButtonDefinition,
      setBackButton
    ]
  );

  useEffect(() => {
    if (receiveRef) {
      receiveRef({
        clickAction,
        clickBack
      });
    }
  }, [receiveRef, clickAction, clickBack]);

  return (
    <ActionButtonContext.Provider value={memoContext}>
      {children}
    </ActionButtonContext.Provider>
  );
}

export function useActionButtonContext() {
  return useContext(ActionButtonContext);
}

export function useActionButtonContextClickSubscription(
  subscriber: ClickSubscriber,
  deps: any[]
) {
  const context = useContext(ActionButtonContext);
  useEffect(() => {
    return context.subscribe(subscriber);
  }, [context.subscribe, ...deps]);
}

export function useActionButtonContextStyleSetter() {
  const context = useActionButtonContext();
  return context.setActionButton;
}

export function useActionButtonContextWithStyle(
  style: SetStateAction<ButtonDefinition | undefined>,
  deps: any[]
) {
  const styleSetter = useActionButtonContextStyleSetter();
  useEffect(() => {
    styleSetter(style);
  }, [styleSetter, ...deps]);
}

export function useActionButtonContextType(type?: ButtonType | "invisible") {
  useActionButtonContextWithStyle(
    oldStyles => ({
      ...oldStyles,
      type
    }),
    [type]
  );
}

export function useActionButtonContextInvisible() {
  useActionButtonContextType("invisible");
}

export function useActionButtonBackInvisible() {
  const { setBackButton } = useActionButtonContext();
  useEffect(() => {
    setBackButton(v => ({
      ...v,
      type: "invisible"
    }));
    return () => {
      setBackButton(v => ({
        ...v,
        type: undefined
      }));
    };
  }, [setBackButton]);
}

export function useActionButtonContextDisabledIf(isDisabled: boolean) {
  useActionButtonContextWithStyle(
    oldStyles => ({
      ...oldStyles,
      disabled: isDisabled
    }),
    [isDisabled]
  );
}

export function useActionButtonContextDisableSetter(
  defaultIsDisabled: boolean
): [boolean, React.Dispatch<SetStateAction<boolean>>] {
  const [isDisabled, setIsDisabled] = useState(defaultIsDisabled);
  useActionButtonContextDisabledIf(isDisabled);

  return [isDisabled, setIsDisabled];
}
