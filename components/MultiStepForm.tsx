import {
  useState,
  SetStateAction,
  useCallback,
  ReactElement,
  useReducer,
  useEffect
} from "react";
import { motion, AnimatePresence } from "framer-motion";
import { useRouter } from "next/router";

type HistoryAction<T> =
  | { type: "push"; payload: T }
  | { type: "pop" }
  | { type: "completeGoingBack" };

function useHistory<T>(...initialStack: T[]) {
  const [state, dispatch] = useReducer(
    (state: { stack: T[]; isGoingBack: boolean }, action: HistoryAction<T>) => {
      switch (action.type) {
        case "pop":
          const [head, ...tail] = state.stack;
          if (tail.length === 0) {
            return state;
          } else {
            return {
              ...state,
              isGoingBack: true,
              stack: tail
            };
          }
        case "completeGoingBack":
          return {
            ...state,
            isGoingBack: false
          };
        case "push":
          return {
            ...state,
            stack: [action.payload, ...state.stack]
          };
      }
    },
    { stack: initialStack, isGoingBack: false }
  );

  const finishGoingBack = useCallback(
    () => dispatch({ type: "completeGoingBack" }),
    [dispatch]
  );

  const pop = useCallback(() => dispatch({ type: "pop" }), [dispatch]);

  const push = useCallback(
    (value: T) => dispatch({ type: "push", payload: value }),
    [dispatch]
  );

  const { stack, isGoingBack } = state;
  const current = stack[0];
  const last = stack[1] as T | undefined;
  return {
    current,
    last,
    isGoingBack,
    finishGoingBack,
    pop,
    push
  };
}

interface StepProps<InputType, StepNames extends string> {
  currentInput: InputType;
  setInput: React.Dispatch<SetStateAction<InputType>>;
  updateInput: <K extends keyof InputType>(key: K, value: InputType[K]) => void;
  previousStep?: StepNames;
  goToStep: (step: StepNames) => void;
  goBack: () => void;
}

type Step<InputType, StepNames extends string> = React.ComponentType<
  StepProps<InputType, StepNames>
>;

interface MultiStepFormProps<InputType, StepNames extends string> {
  initialStep: StepNames;
  initialInput: InputType;
  steps: Record<StepNames, Step<InputType, StepNames>>;
}

export function MultiStepForm<
  InputType extends Object,
  StepNames extends string
>(props: MultiStepFormProps<InputType, StepNames>): ReactElement {
  const { initialStep, initialInput, steps } = props;

  const [input, setInput] = useState(initialInput);

  const updateInput = useCallback(
    <K extends keyof InputType>(key: K, value: InputType[K]) => {
      setInput(v => ({
        ...v,
        [key]: value
      }));
    },
    [setInput]
  );

  const { current, last, finishGoingBack, isGoingBack, pop, push } = useHistory<
    StepNames
  >(initialStep);
  const canGoBack = !!last;

  const CurrentStep: Step<InputType, StepNames> = steps[current];

  const router = useRouter();
  useEffect(() => {
    router.beforePopState(({ url, as, options }) => {
      if (canGoBack) {
        history.pushState({}, "", router.asPath);
        pop();

        return false;
      } else {
        return true;
      }
    });
  }, [router, canGoBack, pop]);

  return (
    <>
      <div className="stepWrapper">
        <AnimatePresence initial={false} onExitComplete={finishGoingBack}>
          <motion.div
            key={current}
            className="step"
            initial={{ x: isGoingBack ? "-110%" : "110%" }}
            animate={{ x: "0%" }}
            exit={{ x: isGoingBack ? "20%" : "-20%" }}
            transition={{ ease: "easeOut", duration: 0.4 }}
          >
            <CurrentStep
              currentInput={input}
              goBack={pop}
              goToStep={push}
              setInput={setInput}
              updateInput={updateInput}
              previousStep={last}
            />
          </motion.div>
        </AnimatePresence>
      </div>
      <style jsx>{`
        .stepWrapper {
          position: relative;
          flex: 1;
          overflow: hidden;
        }
        :global(.step) {
          height: 100%;
          width: 100%;
          overflow auto;
          position: absolute;
          display: flex;
          flex-direction: column;
          background-color: white;
          box-shadow: 0 0 20px rgba(0, 0, 0, 0.2);
        }
      `}</style>
    </>
  );
}
