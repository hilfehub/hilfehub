import MenuButton from "./MenuButton";
import {
  HomeOutlined,
  WifiOutlined,
  EllipsisOutlined,
  UnorderedListOutlined
} from "@ant-design/icons";
import EmptyMenu from "./EmptyMenu";
import { useLogout } from "./TokenContext";
import { useRouter } from "next/router";
import { useCallback } from "react";

interface MenuProps {
  active?: "radar" | "tasks";
}

export default function Menu({ active }: MenuProps) {
  const logout = useLogout();
  const router = useRouter();

  const handleLogout = useCallback(async () => {
    await router.push("/");
    logout();
  }, [logout, router]);

  return (
    <EmptyMenu>
      <MenuButton
        caption="Hilferadar"
        icon={<WifiOutlined />}
        href="/radar"
        active={active == "radar"}
      />
      <MenuButton
        caption="Hilfe"
        icon={<UnorderedListOutlined />}
        href="/tasks"
        active={active == "tasks"}
      />
      <MenuButton
        caption="Logout"
        icon={<EllipsisOutlined />}
        onClick={handleLogout}
      />
    </EmptyMenu>
  );
}
