import NextHead from 'next/head';
import { PropsWithChildren } from 'react';
import clsx from 'clsx';

interface HeadProps {
    title?: string;
    noindex?: boolean;
    nofollow?: boolean;
}

export default function Head({
    title = "",
    noindex = false,
    nofollow = false,
    children
}: PropsWithChildren<HeadProps>) {
    return (
        <>
            <NextHead>
                <title key="title">{!!title ? `${title} \u00B7 HilfeHub` : `HilfeHub \u2013 wo Nachbarschaftshilfe in der Nachbarschaft bleibt`}</title>
                <link rel="icon" type="image/svg+xml" href="/favicon.svg"></link>
                <link rel="canonical" href="https://hilfehub.org/" key="canonical" />
                <link rel="apple-touch-icon" href="/favicon.svg" />
                <meta name="theme-color" content="#70009c" />
                <meta name="apple-mobile-web-app-title" content="HilfeHub" />
                <meta name="apple-mobile-web-app-status-bar-style" content="default" />
                <meta name="apple-mobile-web-app-capable" content="yes" />
                <meta name="mobile-web-app-capable" content="yes" />
                <meta name="viewport" content="width=device-width, initial-scale=1" key="viewport" />
                <meta name="description" content={'HilfeHub bringt Hilfesuchende, Helfende und Datenschutz zusammen \u2013 nicht nur in Zeiten von Corona. Für Besorgungen, Betreuungen, Nachbarschaftshilfe.'} key="description" />
                <meta name="twitter:card" content={'HilfeHub bringt Hilfesuchende, Helfende und Datenschutz zusammen \u2013 denn was in der Nachbarschaftshilfe passiert, soll in der Nachbarschaft bleiben.'} key="twitter:card" />
                <meta name="robots" content={clsx(noindex && "noindex", noindex && nofollow && ",", nofollow && "nofollow")} key="robots" />
                <meta name="google-site-verification" content="p4LJvhknajV255ALu-gM_g6Y211fYvtwAQiPJ4GCXqA" />
                {children}
            </NextHead>
        </>
    )
}