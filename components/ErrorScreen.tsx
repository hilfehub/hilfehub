import { Result, Button } from "antd";

interface ErrorScreenProps {
  onReport: () => void;
  onReload: () => void;
}

export function ErrorScreen(props: ErrorScreenProps) {
  const { onReport, onReload } = props;

  return (
    <Result
      status="500"
      title="Fehler"
      subTitle="Entschuldige, da ist wohl etwas schiefgelaufen."
      extra={[
        <Button type="primary" onClick={onReport}>
          Fehler melden
        </Button>,
        <Button onClick={onReload}>Seite erneut laden</Button>
      ]}
    />
  );
}
