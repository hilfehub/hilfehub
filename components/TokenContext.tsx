import {
  createContext,
  PropsWithChildren,
  useMemo,
  useContext,
  useCallback,
  useEffect
} from "react";
import Axios from "axios";
import { useLocalStorage } from "react-use";
import { TokenResponse } from "../pages/api/token";
import { Sentry } from "./Sentry";
import { RegisteredUser, Address } from "../types";

interface TokenContextValue {
  login: (username: string, password: string) => Promise<RegisteredUser | null>;
  setToken: (token: string, userData: RegisteredUser) => void;
  setPhoneNumber: (number: string) => void;
  setAddress: (address: Address) => void;
  logout: () => void;
  token: string | null;
  user: RegisteredUser | null;
}

const tokenCtx = createContext<TokenContextValue>({
  login: async () => null,
  setToken: () => {},
  logout: () => {},
  setPhoneNumber: () => {},
  setAddress: () => {},
  token: null,
  user: null
});

function fromBase64(v: string): string {
  return atob(v);
}

interface JWTPayload {
  uid: string;
  iat: number;
  exp: number;
}

function getJWTPayload(token: string): JWTPayload {
  const [alg, payloadString, signature] = token.split(".");
  const payload: JWTPayload = JSON.parse(fromBase64(payloadString));
  return payload;
}

function getIssueTime(token: string): Date {
  const { iat } = getJWTPayload(token);
  return new Date(iat * 1000);
}

function getExpiry(token: string): Date {
  const { exp } = getJWTPayload(token);
  return new Date(exp * 1000);
}

function isExpired(token: string): boolean {
  return getExpiry(token) <= new Date();
}

async function fetchToken(oldToken: string): Promise<TokenResponse> {
  const { data } = await Axios.get<TokenResponse>("/api/token", {
    headers: {
      Authorization: `Bearer ${oldToken}`
    }
  });

  return data;
}

async function login(
  username: string,
  password: string
): Promise<TokenResponse> {
  const { data } = await Axios.get<TokenResponse>("/api/token", {
    auth: {
      username,
      password: unescape(encodeURIComponent(password)) // required for non-latin-1 passwords (see https://github.com/axios/axios/issues/1446)
    }
  });

  return data;
}

export function TokenProvider(props: PropsWithChildren<{}>) {
  const [token, setToken] = useLocalStorage<string | undefined>("jwt");
  const [user, setUser] = useLocalStorage<RegisteredUser | undefined>(
    "userData"
  );

  const fetchNewToken = useCallback(async () => {
    if (!token) {
      throw new Error(
        "I shouldn't have been called, since useEffect wouldn't have created a timer. Right?"
      );
    }

    const { token: newToken, user: newUser } = await fetchToken(token);
    setToken(newToken);
    setUser(newUser);
  }, [token, setToken, setUser]);

  useEffect(() => {
    if (!token) {
      return;
    }

    function calculateRefreshDate() {
      const issuedAt = getIssueTime(token!);
      const expiresAt = getExpiry(token!);
      const lifespan = +expiresAt - +issuedAt;
      const percentageOfLifespanToRefreshAfter = 0.8;
      const refreshDistance = lifespan * percentageOfLifespanToRefreshAfter;
      return +issuedAt + refreshDistance;
    }

    function setTimoutDate(cb: () => any, date: number) {
      return setTimeout(cb, date - Date.now());
    }

    const timerId = setTimoutDate(fetchNewToken, calculateRefreshDate());
    return () => clearTimeout(timerId);
  }, [token, fetchNewToken]);

  useEffect(() => {
    Sentry.setUser(user);
  }, [user]);

  const logout = useCallback(() => {
    setToken(undefined);
    setUser(undefined);
  }, [setToken, setUser]);

  const setTokenRoutine = useCallback(
    (token: string, user: RegisteredUser) => {
      setUser(user);
      setToken(token);
    },
    [setToken, setUser]
  );

  const loginRoutine = useCallback(
    async (username: string, password: string) => {
      try {
        const { token, user } = await login(username, password);
        setTokenRoutine(token, user);
        return user;
      } catch (e) {
        return null;
      }
    },
    [setTokenRoutine]
  );

  const setPhoneNumber = useCallback(
    (number: string) => {
      setUser(user =>
        !!user
          ? {
              ...user,
              telephoneNumber: number
            }
          : user
      );
    },
    [setUser]
  );

  const setAddress = useCallback(
    (address: Address) => {
      setUser(user =>
        !!user
          ? {
              ...user,
              address
            }
          : user
      );
    },
    [setUser]
  );

  const value = useMemo<TokenContextValue>(
    () => ({
      logout,
      token: !!token ? (isExpired(token) ? null : token) : null,
      user: !!token ? (isExpired(token) ? null : user!) : null,
      login: loginRoutine,
      setToken: setTokenRoutine,
      setPhoneNumber,
      setAddress
    }),
    [loginRoutine, setTokenRoutine, token, logout, setAddress, setPhoneNumber]
  );

  return <tokenCtx.Provider value={value}>{props.children}</tokenCtx.Provider>;
}

export function useLoginAttempt() {
  const { login } = useContext(tokenCtx);
  return login;
}

export function useLogout() {
  const { logout } = useContext(tokenCtx);
  return logout;
}

export function useTokenSetter() {
  const { setToken } = useContext(tokenCtx);
  return setToken;
}

export function useToken() {
  const { token } = useContext(tokenCtx);
  return token;
}

export function useUserData() {
  const { user } = useContext(tokenCtx);
  return user;
}

export function usePhoneNumberSetter() {
  const { setPhoneNumber } = useContext(tokenCtx);
  return setPhoneNumber;
}

export function useAddressSetter() {
  const { setAddress } = useContext(tokenCtx);
  return setAddress;
}

/**
 * @returns true if logged in
 */
export function useLoginState() {
  const t = useToken();
  return !!t;
}
