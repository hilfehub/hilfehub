import * as _Sentry from "@sentry/browser";
import { RegisteredUser } from "../types";

export module Sentry {
  export function init() {
    _Sentry.init({
      release: process.env.SENTRY_RELEASE,
      dsn: process.env.SENTRY_DSN,
      enabled: process.env.NODE_ENV === "production"
    });
  }

  export function setUser(user?: RegisteredUser) {
    _Sentry.configureScope(scope => {
      if (user) {
        const { _id, email } = user ?? {};
        scope.setUser({ email, id: _id });
      } else {
        scope.setUser({});
      }
    });
  }

  export const showReportDialog = _Sentry.showReportDialog;

  export function captureException(error: any, ctx: any = {}): string {
    _Sentry.configureScope(scope => {
      if (error.message) {
        scope.setFingerprint([error.message]);
      }

      if (error.statusCode) {
        scope.setExtra("statusCode", error.statusCode);
      }

      if (ctx) {
        const { errorInfo, req, res, query, pathname } = ctx;

        if (res && res.statusCode) {
          scope.setExtra("statusCode", res.statusCode);
        }

        scope.setTag("ssr", "false");

        if (errorInfo) {
          scope.setExtras(errorInfo);
        }

        if (typeof window !== "undefined") {
          scope.setTag("ssr", "false");
          scope.setExtra("query", query);
          scope.setExtra("pathname", pathname);
        } else {
          scope.setTag("ssr", "true");

          if (req) {
            scope.setExtra("url", req.url);
            scope.setExtra("method", req.method);
            scope.setExtra("headers", req.headers);
            scope.setExtra("params", req.params);
            scope.setExtra("query", req.query);
          }
        }
      }
    });

    return _Sentry.captureException(error);
  }
}
