import { ReactElement } from "react";
import { Checkbox } from "antd";
import { RightOutlined } from "@ant-design/icons";

interface FilterProps {
  filters: {
    name: string;
    checked: boolean;
    onClick: () => void;
  }[];
}

export default function Filter({ filters }: FilterProps) {
  const createFilters = () => {
    let filterRender: ReactElement[] = [];
    filters.forEach(filter => {
      filterRender.push(
        <Checkbox defaultChecked={filter.checked}>{filter.name}</Checkbox>
      );
    });
    return filterRender;
  };

  return (
    <div className="filter bg-secondary">
      <div className="filterWrapper margin-left">
        <div className="label">Filter</div>
        <div className="checkboxWrapper">{createFilters()}</div>
      </div>
      <div className="arrowWrapper margin-right">
        <RightOutlined />
      </div>
      <style jsx>{`
        .filter {
          background-color: rgba(0, 0, 0, 0.06);
          display: flex;
          align-items: center;
        }
        .label {
          font-size: 0.7rem;
          position: sticky;
          left: 0;
        }
        .checkboxWrapper {
          white-space: nowrap;
        }
        .filterWrapper {
          flex: 1 1 auto;
          overflow-x: auto;
          padding: 0.5rem 0;
        }
        .arrowWrapper {
          font-size: 1.2rem;
          flex: 0 0 auto;
        }
      `}</style>
    </div>
  );
}
