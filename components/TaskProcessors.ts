import { Task, User } from "../types";

interface ProcessedCart {
  productCount: number;
}

export function parseCart(cart?: string): ProcessedCart {
  const processedCart: ProcessedCart = {
    productCount: 0
  };

  processedCart.productCount = (cart?.match(/\n/g) || []).length;
  if (!!cart) processedCart.productCount += 1;

  return processedCart;
}

export function getTaskName(task?: Task) {
  if (!!task)
    switch (task.type) {
      case "care":
        return "Betreuung";
      case "errand":
        return "Besorgungen";
      case "transport":
        return "Transport";
    }
  return "Hilfe";
}

export function getSeeker(task?: Task) {
  if (!task) return undefined;
  return !task.client ? task.creator : task.client;
}

export function getUserName(user?: User) {
  if (!user) return undefined;

  if (user.formalAddress === false) {
    return user.firstname;
  } else {
    if (user.sex === "male") {
      return `Herr ${user.lastname}`;
    } else if (user.sex === "female") {
      return `Frau ${user.lastname}`;
    } else {
      return `${user.firstname} ${user.lastname}`;
    }
  }
}
