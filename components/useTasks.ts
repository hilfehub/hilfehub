import { useAuthenticatedSWR, useAxios } from "./useAPI";
import { useCallback } from "react";
import { Task } from "../types";

export function useTasks(): [Task[], boolean] {
  const { data: tasks, isValidating } = useAuthenticatedSWR<Task[]>("/tasks");
  return [tasks || [], isValidating];
}
export function useRejectTask() {
  const axios = useAxios();

  const rejectTask = useCallback(
    async (taskId: string) => {
      await axios.post(`/api/tasks/${taskId}/reject`);
    },
    [axios]
  );

  return rejectTask;
}

export function useTask(
  taskId: string
): [Task | undefined, boolean, () => Promise<boolean>] {
  const { data: task, isValidating, revalidate } = useAuthenticatedSWR<Task>(
    `/tasks/${taskId}`
  );
  return [task || undefined, isValidating, revalidate];
}

export function usePickedTasks(): [Task[], boolean] {
  const { data: tasks, isValidating } = useAuthenticatedSWR<Task[]>(
    "/tasks/picked"
  );
  return [tasks || [], isValidating];
}

export function useCreatedTasks(): [Task[], boolean] {
  const { data: tasks, isValidating } = useAuthenticatedSWR<Task[]>(
    "/tasks/created"
  );
  return [tasks || [], isValidating];
}

type TaskPicker = (taskId: string) => Promise<void>;

export function useTaskPicker(): TaskPicker {
  const axios = useAxios();
  return useCallback(
    async (taskId: string) => {
      await axios.post(`/tasks/${taskId}/pick`);
    },
    [axios]
  );
}

type TaskRejecter = (taskId: string) => Promise<void>;

export function useTaskRejecter(): TaskRejecter {
  const axios = useAxios();
  return useCallback(
    async (taskId: string) => {
      await axios.post(`/tasks/${taskId}/reject`);
    },
    [axios]
  );
}

type TaskDeleter = (taskId: string) => Promise<void>;

export function useTaskDeleter(): TaskDeleter {
  const axios = useAxios();
  return useCallback(
    async (taskId: string) => {
      await axios.delete(`/tasks/${taskId}`);
    },
    [axios]
  );
}

type TaskCompleter = (taskId: string) => Promise<void>;

export function useTaskCompleter(): TaskCompleter {
  const axios = useAxios();
  return useCallback(
    async (taskId: string) => {
      await axios.post(`/tasks/${taskId}/complete`);
    },
    [axios]
  );
}
