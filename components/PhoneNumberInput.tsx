import PhoneInput from "react-phone-input-2";
import "react-phone-input-2/lib/semantic-ui.css";
import * as _ from "lodash";
import { PhoneNumber } from "../util/PhoneNumber";

interface PhoneNumberInputProps {
  country?: string;
  preferredCountries?: string[];
  placeholder?: string;
  value?: string;
  onChange?: (number: string) => void;
  onChangeIsValid?: (valid: boolean) => void;
}

export function PhoneNumberInput(props: PhoneNumberInputProps) {
  const { onChange = () => {}, onChangeIsValid = () => {}, ...other } = props;

  return (
    <PhoneInput
      {...other}
      onChange={(v: string) => {
        onChange(v);
        onChangeIsValid(PhoneNumber.isValid(v));
      }}
    />
  );
}
