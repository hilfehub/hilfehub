import { PureComponent } from "react";
import { ErrorScreen } from "./ErrorScreen";
import { Sentry } from "./Sentry";

Sentry.init();

interface ErrorBoundaryState {
  hasError: boolean;
  errorEventId?: string;
}

class ErrorBoundary extends PureComponent<{}, ErrorBoundaryState> {
  state: Readonly<ErrorBoundaryState> = {
    hasError: false,
    errorEventId: undefined
  };

  static getDerivedStateFromError(): Partial<ErrorBoundaryState> {
    return { hasError: true };
  }

  componentDidCatch(error: any, errorInfo: any) {
    const errorEventId = Sentry.captureException(error, { errorInfo });
    this.setState({ errorEventId });
  }

  render() {
    const { errorEventId, hasError } = this.state;

    if (hasError) {
      return (
        <ErrorScreen
          onReport={() => Sentry.showReportDialog({ eventId: errorEventId })}
          onReload={() => window.location.reload(true)}
        />
      );
    }

    return this.props.children;
  }
}

export function withErrorBoundary<T>(Component: React.ComponentType<T>) {
  return function WithErrorBoundary(props: T) {
    return (
      <ErrorBoundary>
        <Component {...props} />
      </ErrorBoundary>
    );
  };
}
