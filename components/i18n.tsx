import { setupI18n } from "@lingui/core";
import { I18nProvider as _I18nProvider } from "@lingui/react";
import catalogEn from "../locales/en/messages";

export const i18n = setupI18n();

export function I18nProvider(props: React.PropsWithChildren<{}>) {
  return (
    <_I18nProvider
      i18n={i18n}
      language="de"
      catalogs={{
        en: catalogEn
      }}
    >
      {props.children}
    </_I18nProvider>
  );
}
