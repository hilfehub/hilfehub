import { AutoSizer } from "react-virtualized";
import "react-virtualized/styles.css";
import "mapbox-gl/dist/mapbox-gl.css";
import ReactMapGL, { InteractiveMapProps } from "react-map-gl";

type AutoSizedMapboxMapProps = Omit<
  React.PropsWithChildren<InteractiveMapProps>,
  "height" | "width"
>;

export default function AutoSizedMapboxMap(props: AutoSizedMapboxMapProps) {
  return (
    <AutoSizer>
      {({ height, width }) => (
        <ReactMapGL {...props} height={height} width={width} />
      )}
    </AutoSizer>
  );
}
