import { Tabs } from "antd";
import LoginForm from "./LoginForm";
import SignUpForm from "./SignUpForm";
import { SignupData } from "../backend";

interface FormProps {
  header?: string;
  intro?: string;
  firstPane: "signup" | "login";
  onSignupDataChange: (v: SignupData | null) => void;
  onLoginCredentialsChange: (v: [string, string]) => void;
  onActivePaneChanged: (v: "signup" | "login") => void;
  onSubmit?: () => void;
}

const { TabPane } = Tabs;

export default function FormCredentials({
  header,
  intro,
  onLoginCredentialsChange,
  onSignupDataChange,
  onActivePaneChanged,
  firstPane,
  onSubmit
}: FormProps) {
  const signupPane = (
    <TabPane tab="Registrieren" key="signup">
      <SignUpForm onChange={onSignupDataChange} onSubmit={onSubmit} />
    </TabPane>
  );

  const loginPane = (
    <TabPane tab="Einloggen" key="login">
      <LoginForm onChange={onLoginCredentialsChange} onSubmit={onSubmit} />
    </TabPane>
  );

  return (
    <div className="credentialForm">
      {header && <h1 className="margin-top">{header}</h1>}
      {intro && <h3>{intro}</h3>}
      <Tabs
        defaultActiveKey={firstPane}
        onChange={k => onActivePaneChanged(k as "signup" | "login")}
      >
        {firstPane === "login" ? loginPane : signupPane}
        {firstPane === "login" ? signupPane : loginPane}
      </Tabs>
    </div>
  );
}
