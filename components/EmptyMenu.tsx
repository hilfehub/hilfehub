interface EmptyMenuProps {
  children?: JSX.Element | JSX.Element[] | never[];
}

export default function EmptyMenu({ children }: EmptyMenuProps) {
  return (
    <nav className="noselect" style={{ zIndex: 10 }}>
      <div className="menu extra-margins-sides">{children}</div>
      <style jsx>{`
        nav {
          box-shadow: 0 0 10px rgba(0, 0, 0, 0.15);
        }
        .menu {
          grid-area: menu;
          display: flex;
          align-self: center;
          height: 100%;
          z-index: 99;
        }
        .menu :global(.padding-around) {
          padding-top: 0.25rem;
          padding-bottom: 0.25rem;
        }
      `}</style>
    </nav>
  );
}
