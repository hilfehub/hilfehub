import Link from "next/link";
import clsx from "clsx";

interface MenuButtonProps {
  caption?: string;
  icon: JSX.Element;
  href?: string;
  active?: boolean;
  onClick?: () => void;
}

function MenuButton({ caption, icon, href, active, onClick }: MenuButtonProps) {
  const className: string = active ? "active" : "";
  href = active ? "" : href;

  const contents = (
    <a
      className={clsx("padding-around", className)}
      onClick={!!href ? undefined : onClick}
    >
      <div>
        <div className="icon">{icon}</div>
        {caption && <div className="caption">{caption}</div>}
      </div>
      <style jsx>{`
        a {
          flex: 1 0 0;
          height: 100%;
          display: grid;
          transition: all 0.1s linear;
        }
        a.active {
          cursor: default;
          background-color: rgba(0, 0, 0, 0.05);
        }
        a:active:not(.active),
        a:hover:not(.active) {
          background-color: rgba(0, 0, 0, 0.025);
        }
        a > div {
          align-self: center;
          text-align: center;
        }
        .icon {
          font-size: 1.3rem;
          line-height: 1.3rem;
        }
        .caption {
          font-size: 0.9rem;
          line-height: 0.9rem;
        }
      `}</style>
    </a>
  );

  return !!href ? <Link href={href}>{contents}</Link> : contents;
}

export default MenuButton;
