import { RightOutlined } from "@ant-design/icons";
import Link from "next/link";
import { ReactElement } from "react";

interface ItemProps {
  name: string;
  caption: string;
  href?: string;
  as?: string;
  icon?: ReactElement;
}

export default function ListItem({ name, caption, href, as, icon }: ItemProps) {
  const baseItem = (
    <>
      {icon && <div className="iconWrapper margin-left">{icon}</div>}
      <div
        className={
          !icon
            ? !href
              ? "contentWrapper margin-left margin-right"
              : "contentWrapper margin-left"
            : !href
            ? "contentWrapper margin-right"
            : "contentWrapper"
        }
      >
        <div className="nameWrapper">{name}</div>
        <div className="captionWrapper">{caption}</div>
      </div>
    </>
  );

  const wrapLink = (content: ReactElement) => (
    <>
      <Link href={href!} as={as}>
        <a className="item noselect no-link-style">
          {content}
          <div className="arrowWrapper margin-right">
            <RightOutlined />
          </div>
        </a>
      </Link>
    </>
  );

  const wrapItem = (content: ReactElement) => (
    <>
      <div className="item">{content}</div>
    </>
  );

  return (
    <>
      {!href ? wrapItem(baseItem) : wrapLink(baseItem)}
      <style jsx>{`
        :global(.item) {
          display: flex;
          flex-direction: row;
          padding: 0.5rem 0;
          background-color: rgba(0, 0, 0, 0);
        }
        :global(a.item) {
          cursor: pointer;
        }
        :global(a.item:hover) {
          background-color: rgba(0, 0, 0, 0.03);
        }
        :global(a.item:active) {
          background-color: rgba(0, 0, 0, 0.06);
        }
        :global(.item .contentWrapper) {
          flex: 1 0 0;
        }
        :global(.item .arrowWrapper) {
          font-size: 1.5rem;
          display: flex;
          align-items: center;
          margin-left: 0.5rem;
        }
        :global(.item .iconWrapper) {
          font-size: 1.5rem;
          display: flex;
          align-items: center;
          margin-right: 0.5rem;
        }
        :global(.item .nameWrapper) {
          font-weight: bold;
          font-size: 0.95rem;
        }
        :global(.item .captionWrapper) {
          font-size: 0.8rem;
          white-space: pre-wrap;
        }
      `}</style>
    </>
  );
}
