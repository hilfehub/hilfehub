import { Form, Button } from "antd";

export function InvisibleSubmitButton() {
  return (
    <Form.Item>
      <Button htmlType="submit" style={{ left: "-9999px" }}>
        Log in
      </Button>
    </Form.Item>
  );
}
