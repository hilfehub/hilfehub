import { PropsWithChildren, ReactChild } from "react";
import { LoadingOutlined } from "@ant-design/icons";

interface LoadingSpinnerProps {
  isLoading: boolean;
  loadingMessage?: string;
}

export default function LoadingSpinner({
  isLoading,
  loadingMessage = "Lade ...",
  children
}: PropsWithChildren<LoadingSpinnerProps>) {
  if (isLoading) {
    return (
      <div style={{ textAlign: "center", marginTop: "2rem" }}>
        <LoadingOutlined style={{ fontSize: "2rem", marginBottom: "0.5rem" }} />
        <br />
        {loadingMessage}
      </div>
    );
  }

  return <>{children}</>;
}
