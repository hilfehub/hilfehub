import { Button, Col } from "antd";
import { LeftOutlined } from "@ant-design/icons";
import EmptyMenu from "./EmptyMenu";
import { ButtonType } from "antd/lib/button";

interface MenuProps {
  type?: ButtonType | "invisible";
  disabled?: boolean;
  caption?: React.ReactChild;
  onClick?: () => void;
  onClickBack?: () => void;
  backVisible?: boolean;
}

export default function ActionButtonMenu({
  caption,
  type = "primary",
  disabled = false,
  onClick,
  onClickBack = () => window.history.back(),
  backVisible = true
}: MenuProps) {
  return (
    <EmptyMenu>
      <div className="padding-around buttonWrapper">
        <Col span={7}>
          <Button
            size="large"
            onClick={onClickBack}
            block
            style={{ display: backVisible ? undefined : "none" }}
          >
            <LeftOutlined />
          </Button>
        </Col>
        <Col span={16}>
          <Button
            size="large"
            type={type === "invisible" ? "primary" : type}
            onClick={onClick}
            disabled={disabled || type === "invisible"}
            style={{ display: type === "invisible" ? "none" : undefined }}
            block
          >
            {caption}
          </Button>
        </Col>
      </div>
      <style jsx>{`
        .buttonWrapper {
          display: flex;
          width: 100%;
          padding: 0 2em;
          align-items: center;
          justify-content: space-between;
        }
      `}</style>
    </EmptyMenu>
  );
}
