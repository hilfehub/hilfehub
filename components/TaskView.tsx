import Item from "./ListItem";
import Divider from "./ListDivider";
import Head from "./Head";
import { Button, Modal } from "antd";
import {
  EnvironmentOutlined,
  QuestionOutlined,
  EllipsisOutlined,
  UserOutlined
} from "@ant-design/icons";
import {
  getTaskName,
  getSeeker,
  getUserName
} from "../components/TaskProcessors";
import { useRejectTask, useTaskDeleter } from "./useTasks";
import { CareTask, TransportTask, ErrandTask, Task } from "../types";

type WithReload<Props> = Props & {
  onReload: () => void;
};

interface TaskViewProps<TaskType extends Task> {
  task: TaskType;
}

export function ErrandTaskItems({ task }: TaskViewProps<ErrandTask>) {
  return (
    <>
      <Item
        name={task.shopMapboxData.place_name}
        caption={task.shopMapboxData.properties.address ?? ""}
        icon={<EnvironmentOutlined />}
      ></Item>
      <Item
        name="Einkaufsliste"
        caption={task.cart}
        icon={<QuestionOutlined />}
      ></Item>
    </>
  );
}

export function CareTaskItems({ task }: TaskViewProps<CareTask>) {
  return (
    <>
      <Item
        name={"Zu betreuende Person(en)"}
        caption={task.description}
        icon={<QuestionOutlined />}
      ></Item>
    </>
  );
}

export function TransportTaskItems({ task }: TaskViewProps<TransportTask>) {
  return (
    <>
      <Item
        name={"Start"}
        caption={`${task.start.streetAndNumber}, ${task.start.zipCode} ${task.start.town}`}
        icon={<EnvironmentOutlined />}
      ></Item>
      <Item
        name={"Ziel"}
        caption={`${task.destination.streetAndNumber}, ${task.destination.zipCode} ${task.destination.town}`}
        icon={<EnvironmentOutlined />}
      ></Item>
      <Item
        name={"Zu transportieren/befördern"}
        caption={task.subject}
        icon={<QuestionOutlined />}
      ></Item>
    </>
  );
}

function TaskItems({ task }: TaskViewProps<Task>) {
  switch (task.type) {
    case "care":
      return <CareTaskItems task={task} />;
    case "errand":
      return <ErrandTaskItems task={task} />;
    case "transport":
      return <TransportTaskItems task={task} />;
    default:
      return null;
  }
}

function NameTaskItem({ task }: TaskViewProps<Task>) {
  return (
    <>
      {task?.client?.isQuarantined && <Divider title="In Quarantäne" />}
      <Item
        name={`Für ${getUserName(getSeeker(task)) ||
          "eine hilfebedürftige Person"}`}
        caption={`${getSeeker(task)!.telephoneNumber}\n${
          getSeeker(task)!.address?.streetAndNumber
        }, ${getSeeker(task)!.address?.zipCode} ${
          getSeeker(task)!.address?.town
        }`}
        icon={<UserOutlined />}
      ></Item>
    </>
  );
}

function FurtherInformationTaskItem({
  task,
  header
}: TaskViewProps<Task> & { header: string }) {
  return (
    <>
      {!!task.furtherInformation && (
        <Item
          name={header}
          caption={task.furtherInformation}
          icon={<EllipsisOutlined />}
        ></Item>
      )}
    </>
  );
}

export function DefaultTaskView({ task }: WithReload<TaskViewProps<Task>>) {
  return (
    <>
      <Head title="Unbeantwortete Aufgabe" />
      <h1 className="margins-sides margin-top">{getTaskName(task)}</h1>
      <TaskItems task={task} />
      <FurtherInformationTaskItem task={task} header={`Bemerkungen`} />
    </>
  );
}

function showConfirmRejectionModal(rejectHandle: () => Promise<void>) {
  Modal.confirm({
    title: "Möchtest du die Aufgabe wirklich abgeben?",
    icon: <QuestionOutlined />,
    content:
      "Die Aufgabe wird wieder für alle sichtbar auf der Karte angezeigt und kann von jedem angenommen werden.",
    okText: "Aufgabe abgeben",
    okButtonProps: { type: "danger", size: "large" },
    cancelText: "Abbrechen",
    cancelButtonProps: { size: "large" },
    centered: true,
    async onOk() {
      try {
        await rejectHandle();
      } catch {
        Modal.error({
          title: "Bei der Abgabe der Aufgabe ist ein Fehler aufgetreten",
          content:
            "Probieren Sie es erneut. Sollten Sie die Aufgabe nicht abgeben können, kontaktieren Sie bitte die Seitenbetreiber."
        });
      }
    },
    onCancel() {}
  });
}

export function PickerTaskView({
  task,
  onReload
}: WithReload<TaskViewProps<Task>>) {
  const rejectTask = useRejectTask();
  return (
    <>
      <Head title={`${getTaskName(task)} für ${getUserName(getSeeker(task))}`} />
      <h1 className="margins-sides margin-top">{getTaskName(task)}</h1>
      <NameTaskItem task={task} />
      <TaskItems task={task} />
      <FurtherInformationTaskItem
        task={task}
        header={`Bemerkungen von ${getUserName(getSeeker(task)) ||
          "der hilfebedürftigen Person"}`}
      />
      <div className="margins">
        <Button
          type="ghost"
          size="large"
          danger
          block
          onClick={e =>
            showConfirmRejectionModal(async () => {
              await rejectTask(task._id);
              onReload();
            })
          }
        >
          Aufgabe abgeben
        </Button>
      </div>
    </>
  );
}

function showConfirmDeletionModal(deleteHandle: () => Promise<void>) {
  Modal.confirm({
    title: "Möchtest du die Aufgabe wirklich löschen?",
    icon: <QuestionOutlined />,
    content:
      "Die Aufgabe wird gelöscht. Dieser Vorgang kann nicht rückgängig gemacht werden.",
    okText: "Unwiderruflich löschen",
    okButtonProps: { type: "danger", size: "large" },
    cancelText: "Abbrechen",
    cancelButtonProps: { size: "large" },
    centered: true,
    async onOk() {
      try {
        await deleteHandle();
      } catch {
        Modal.error({
          title: "Bei der Löschung der Aufgabe ist ein Fehler aufgetreten",
          content:
            "Probieren Sie es erneut. Sollten Sie die Aufgabe nicht löschen können, kontaktieren Sie bitte die Seitenbetreiber."
        });
      }
    },
    onCancel() {}
  });
}

export function CreatorTaskView({
  task,
  onReload
}: WithReload<TaskViewProps<Task>>) {
  const deleteTask = useTaskDeleter();

  return (
    <>
      <Head title={`${getTaskName(task)} für dich`} />
      <h1 className="margins-sides margin-top">{getTaskName(task)}</h1>
      <NameTaskItem task={task} />
      <TaskItems task={task} />
      <FurtherInformationTaskItem
        task={task}
        header={`Bemerkungen von ${getUserName(getSeeker(task)) ||
          "der hilfebedürftigen Person"}`}
      />
      <div className="margins">
        <Button
          type="ghost"
          size="large"
          danger
          block
          onClick={e =>
            showConfirmDeletionModal(async () => {
              await deleteTask(task._id);
              onReload();
            })
          }
        >
          Aufgabe löschen
        </Button>
      </div>
    </>
  );
}
