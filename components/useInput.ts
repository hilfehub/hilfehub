import { useState, useCallback, ChangeEvent, ChangeEventHandler } from "react";

export function useInput(
  defaultVal: string = ""
): [string, ChangeEventHandler] {
  const [state, setState] = useState(defaultVal);
  const updater = useCallback(
    (evt: ChangeEvent<HTMLInputElement>) => {
      setState(evt.target.value);
    },
    [setState]
  );
  return [state, updater];
}
