import FormCredentials from "./FormCredentials";
import { useLoginAttempt, useTokenSetter } from "./TokenContext";
import { useState, useCallback, useEffect } from "react";
import { SignupData } from "../backend";
import Axios from "axios";
import { SignupResponse } from "../pages/api/signup";
import { notification } from "antd";
import { useActionButtonContext } from "./ActionButtonContext";
import { i18n } from "@lingui/core";
import { t } from "@lingui/macro";
import { RegisteredUser } from "../types";

interface LoginSignupPageProps {
  firstPane: "login" | "signup";
  showHeader?: boolean;

  onInputValid: () => void;
  onInputInvalid: () => void;

  onLoginLoading: () => void;
  onLoginComplete: () => void;
  onLoginSuccessful: (user: RegisteredUser) => void;

  onSignupLoading: () => void;
  onSignupComplete: () => void;
  onSignupSucessful: (user: RegisteredUser) => void;

  onPaneChanged?: (pane: "signup" | "login") => void;
}

export function LoginSignupPage(props: LoginSignupPageProps) {
  const {
    firstPane,
    onSignupComplete,
    onLoginComplete,
    onInputValid,
    onInputInvalid,
    onLoginLoading,
    onLoginSuccessful,
    onSignupLoading,
    onSignupSucessful,
    onPaneChanged = () => {},
    showHeader
  } = props;

  const login = useLoginAttempt();
  const setToken = useTokenSetter();
  const [requestRunning, setRequestRunning] = useState(false);

  const [signupData, setSignupData] = useState<SignupData | null>(null);
  const [[email, password], setLoginCredentials] = useState<[string, string]>([
    "",
    ""
  ]);
  const [activePane, setActivePane] = useState<"signup" | "login">();

  const handleSignup = useCallback(() => {
    async function signup() {
      setRequestRunning(true);
      onSignupLoading();

      const { data, status } = await Axios.post<SignupResponse>(
        "/api/signup",
        signupData,
        {
          validateStatus: status => [200, 409].includes(status)
        }
      );

      setRequestRunning(false);
      onSignupComplete();

      switch (status) {
        case 200:
          setToken(data.token, data.user);
          onSignupSucessful(data.user);
          break;
        case 409:
          notification.warn({
            message: "Diese E-Mail wird bereits verwendet.",
            description: "Bitte verwenden sie eine andere Addresse."
          });
          break;
      }
    }

    signup();
  }, [signupData, setToken, setRequestRunning]);

  const handleLogin = useCallback(() => {
    async function doIt() {
      setRequestRunning(true);
      onLoginLoading();

      const user = await login(email, password);
      const wasSuccessful = !!user;

      setRequestRunning(false);
      onLoginComplete();

      if (wasSuccessful) {
        onLoginSuccessful(user!);
      } else {
        notification.error({
          message: i18n._(t`E-Mail oder Passwort falsch`),
          description: i18n._(
            t`Hat sich eventuell ein Tippfehler eingeschlichen?`
          )
        });
      }
    }

    doIt();
  }, [email, password, onLoginComplete, setRequestRunning]);

  const loginValid = !!email;
  const signupValid = !!signupData;
  const activeFormValid = activePane === "login" ? loginValid : signupValid;
  useEffect(() => {
    if (activeFormValid) {
      onInputValid();
    } else {
      onInputInvalid();
    }
  }, [onInputInvalid, onInputValid, activeFormValid]);

  const enableAction = activeFormValid && !requestRunning;

  const action = activePane === "login" ? handleLogin : handleSignup;

  const context = useActionButtonContext();
  useEffect(
    () =>
      context.subscribe({
        actionButtonClicked: action,
        backButtonClicked: () => {}
      }),
    [action, context.subscribe]
  );

  return (
    <FormCredentials
      header={showHeader ? "Du Held!" : undefined}
      intro={
        showHeader
          ? "Wir finden es super, dass du anderen helfen möchtest! Bevor du loslegen kannst, brauchen wir noch ein paar Informationen über dich."
          : undefined
      }
      firstPane={firstPane || "signup"}
      onActivePaneChanged={activePane => {
        setActivePane(activePane);
        onPaneChanged(activePane);
      }}
      onLoginCredentialsChange={setLoginCredentials}
      onSignupDataChange={setSignupData}
      onSubmit={enableAction ? action : undefined}
    />
  );
}
