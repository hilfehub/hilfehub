import { Row, Col, Input, Form } from "antd";
import { useCallback } from "react";
import { SignupData } from "../backend";
import { InvisibleSubmitButton } from "./InvisibleSubmitButton";
import { FormInstance } from "antd/lib/form";
import Link from "next/link";

async function getValidationErrors(form: FormInstance): Promise<unknown[]> {
  const values = form.getFieldsValue();
  const setFields = Object.entries(values)
    .filter(([key, value]: [string, any]) => value !== undefined)
    .map(e => e[0]);

  try {
    await form.validateFields(setFields);
    return [];
  } catch (result) {
    const { errorFields } = result;
    return errorFields;
  }
}

export async function isFormValid(form: FormInstance) {
  const errors = await getValidationErrors(form);
  return errors.length === 0;
}

interface SignUpFormProps {
  onChange: (data: SignupData | null) => void;
  onSubmit?: () => void;
}

export default function SignUpForm(props: SignUpFormProps) {
  const { onChange, onSubmit } = props;
  const [form] = Form.useForm();

  const handleValuesChange = useCallback(
    async (changedValues: any, values: any) => {
      const formIsValid = await isFormValid(form);
      if (!formIsValid) {
        onChange(null);
        return;
      }

      const { firstname, lastname, email, password } = values;
      const allNeededValuesPresent =
        !!firstname && !!lastname && !!email && !!password;

      if (!allNeededValuesPresent) {
        onChange(null);
      } else {
        onChange({ firstname, lastname, email, password });
      }
    },
    [onChange, form]
  );

  return (
    <Form
      size="large"
      scrollToFirstError
      form={form}
      onFinish={onSubmit}
      onValuesChange={handleValuesChange}
      style={{ paddingTop: "12px" }}
    >
      <Row gutter={24}>
        <Col span={12}>
          <Form.Item
            name="firstname"
            rules={[
              {
                required: true,
                message: "Bitte geben Sie ihren Vornamen an."
              }
            ]}
          >
            <Input placeholder="Vorname" />
          </Form.Item>
        </Col>
        <Col span={12}>
          <Form.Item
            name="lastname"
            rules={[
              {
                required: true,
                message: "Bitte geben Sie ihren Nachnamen an."
              }
            ]}
          >
            <Input placeholder="Nachname" />
          </Form.Item>
        </Col>
      </Row>

      <Row>
        <Col>
          <Form.Item
            name="email"
            dependencies={["email-repeat"]}
            rules={[
              {
                required: true,
                message: "Bitte geben Sie eine E-Mail-Addresse an."
              },
              {
                message: "Die Eingabe ist keine valide E-Mail.",
                type: "email"
              }
            ]}
          >
            <Input placeholder="E-Mail-Addresse" autoComplete="username" />
          </Form.Item>
        </Col>
      </Row>

      <Row>
        <Col>
          <Form.Item
            name="email-repeat"
            dependencies={["email"]}
            rules={[
              {
                required: true,
                message: "Bitte geben wiederholen sie ihre E-Mail-Addresse."
              },
              ({ getFieldValue }) => ({
                async validator(rule, value) {
                  if (!value) {
                    return;
                  }

                  const isValid = getFieldValue("email") === value;

                  if (!isValid) {
                    throw "Die E-Mail-Addressen stimmen nicht überein.";
                  }
                }
              })
            ]}
          >
            <Input placeholder="E-Mail-Addresse wiederholen" />
          </Form.Item>
        </Col>
      </Row>

      <Row>
        <Col>
          <Form.Item
            name="password"
            rules={[
              {
                required: true,
                message: "Bitte setzen Sie ein Passwort."
              }
            ]}
          >
            <Input.Password
              placeholder="Passwort"
              autoComplete="new-password"
            />
          </Form.Item>
        </Col>
      </Row>

      <Row>
        <Col>
          Wir nutzen alles, was wir über dich erfahren, nur, um die Aufgaben-
          und Kontaktvermittlung abzuwickeln und im Nachhinein Statistiken
          anzufertigen. Wir geben diese Daten, wenn überhaupt, nur an Helfende
          weiter. Mehr dazu findest du in unserer{" "}
          <Link href="/privacy-policy">Datenschutzerklärung</Link>.
        </Col>
      </Row>

      <InvisibleSubmitButton />
    </Form>
  );
}
