import { RightOutlined } from "@ant-design/icons";

interface DividerProps {
  title: string;
}

export default function ListDivider({ title }: DividerProps) {
  return (
    <div className="divider noselect">
      <div className="margins-sides">{title}</div>
      <style jsx>{`
        .divider {
          background-color: rgba(0, 0, 0, 0.02);
          font-size: 0.8rem;
          margin-top: 0.5rem;
          padding: 0.2rem 0;
        }
      `}</style>
    </div>
  );
}
