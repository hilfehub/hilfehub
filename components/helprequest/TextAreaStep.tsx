import { StepLayoutProps, StepLayout } from "./StepLayout";
import { Input } from "antd";
import { useEffect } from "react";

interface TextAreaStepProps extends StepLayoutProps {
  placeholder?: string;
  minRows?: number;
  maxRows?: number;
  defaultValues?: {text?: string};
  onChange: (input: string) => void;
}

export function TextAreaStep({
  placeholder,
  minRows = 6,
  maxRows,
  onChange = () => {},
  defaultValues = {text: ""},
  ...stepProps
}: TextAreaStepProps) {
  return (
    <StepLayout {...stepProps}>
      <Input.TextArea
        autoSize={{ minRows: minRows, maxRows: maxRows }}
        placeholder={placeholder}
        onChange={e => onChange(e.target.value)}
        defaultValue={defaultValues.text}
      />
    </StepLayout>
  );
}
