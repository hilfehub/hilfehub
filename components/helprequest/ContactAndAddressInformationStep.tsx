import { Addresses } from "../../backend";
import { Form, Input } from "antd";
import { StepLayoutProps, StepLayout } from "./StepLayout";
import { isFormValid } from "../SignUpForm";
import { PhoneNumberInput } from "../PhoneNumberInput";
import { useLoginState } from "../TokenContext";
import { ExistingUnregisteredUserPicker } from "./ExistingUnregisteredUserPicker";
import { AddressForm } from "./AddressForm";
import { UnregisteredUser } from "../../types";
import { PhoneNumber } from "../../util/PhoneNumber";

export type ContactAndAddressInformation = {
  firstname: string;
  lastname: string;
  phone: string;
  address: Addresses.Create.Payload;
};

interface ContactAndAddressInformationStepProps extends StepLayoutProps {
  onChange: (contactInformation: ContactAndAddressInformation | null) => void;
  onPickExistingUser: (user: UnregisteredUser) => void;
}

export function ContactAndAddressInformationStep({
  onChange,
  onPickExistingUser,
  ...stepProps
}: ContactAndAddressInformationStepProps) {
  const [form] = Form.useForm();

  const isLoggedIn = useLoginState();

  return (
    <StepLayout {...stepProps}>
      {isLoggedIn && (
        <ExistingUnregisteredUserPicker onPick={onPickExistingUser} />
      )}

      <Form
        form={form}
        onValuesChange={async () => {
          const formIsValid = await isFormValid(form);
          if (!formIsValid) {
            onChange(null);
            return;
          }

          const { firstname, lastname, phone, address } = form.getFieldsValue();
          const allNeededPresent =
            !!firstname && !!lastname && !!phone && !!address;
          if (!allNeededPresent) {
            onChange(null);
          } else {
            onChange({
              firstname,
              lastname,
              phone,
              address
            });
          }
        }}
      >
        <Form.Item
          name="firstname"
          rules={[
            {
              required: true,
              message: "Bitte geben Sie einen Vornamen ein."
            }
          ]}
        >
          <Input placeholder="Vorname" />
        </Form.Item>
        <Form.Item
          name="lastname"
          rules={[
            {
              required: true,
              message: "Bitte geben Sie einen Nachnamen ein."
            }
          ]}
        >
          <Input placeholder="Nachname" />
        </Form.Item>
        <Form.Item
          name="phone"
          rules={[
            {
              required: true,
              message: "Bitte geben Sie eine Telefonnummer an."
            },
            {
              async validator(rule, value) {
                if (!PhoneNumber.isValid(value)) {
                  throw "Bitte geben Sie eine gültige Telefonnummer an.";
                }
              }
            }
          ]}
        >
          <PhoneNumberInput
            country="de"
            preferredCountries={["de"]}
            placeholder="Telefonnummer"
          />
        </Form.Item>

        <Form.Item name="address">
          <AddressForm />
        </Form.Item>
      </Form>
    </StepLayout>
  );
}
