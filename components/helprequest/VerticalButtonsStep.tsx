import Button, { ButtonType } from "antd/lib/button";
import { ReactNode, ReactChild } from "react";
import { StepLayoutProps, StepLayout } from "./StepLayout";

export type ButtonProps = {
  caption?: ReactChild;
  icon?: ReactNode;
  type?: ButtonType | "invisible";
  disabled?: boolean;
  onClick?: () => void;
};

interface VerticalButtonsStepProps extends StepLayoutProps {
  buttons: ButtonProps[];
}

export function VerticalButtonsStep({
  buttons,
  ...stepLayoutProps
}: VerticalButtonsStepProps) {
  return (
    <StepLayout {...stepLayoutProps}>
      <div className="centerHorizontal">
        <div id="buttonWrapper">
          {buttons.map((button, index) => (
            <Button
              key={index}
              size="large"
              icon={button.icon}
              type={
                button.type === undefined
                  ? "primary"
                  : button.type === "invisible"
                  ? "primary"
                  : button.type
              }
              disabled={button.disabled || button.type === "invisible"}
              style={{ opacity: button.type === "invisible" ? "0" : "1" }}
              onClick={button.onClick}
              block
            >
              {button.caption}
            </Button>
          ))}
        </div>
      </div>
      <style jsx>{`
        .centerHorizontal {
          display: flex;
          justify-content: center;
          flex: 1;
        }
        #buttonWrapper {
          display: flex;
          flex-direction: column;
          width: 70%;
          text-align: center;
          justify-content: center;
        }
        * > :global(button) {
          margin-top: 0.8rem;
          height: auto;
          padding: 1rem;
          font-size: 1.3rem;
        }
      `}</style>
    </StepLayout>
  );
}
