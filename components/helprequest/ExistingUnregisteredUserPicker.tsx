import { useAuthenticatedSWR } from "../useAPI";
import { useLoginState } from "../TokenContext";
import { List, Avatar } from "antd";
import { UnregisteredUser } from "../../types";

function useUnregisteredUsers(): [UnregisteredUser[], boolean] {
  const isLoggedIn = useLoginState();
  const { data, isValidating } = useAuthenticatedSWR<UnregisteredUser[]>(
    isLoggedIn ? "/users/unregistered" : null
  );
  return [data ?? [], isValidating];
}

interface ExistingUnregisteredUserPickerProps {
  onPick: (user: UnregisteredUser) => void;
}

export function ExistingUnregisteredUserPicker(
  props: ExistingUnregisteredUserPickerProps
) {
  const { onPick } = props;

  const [existingUnregisteredUsers, isLoading] = useUnregisteredUsers();

  if (existingUnregisteredUsers.length === 0) {
    return null;
  }

  return (
    <div
      style={{
        maxWidth: "500px",
        margin: "0 auto",
        paddingTop: "1em",
        paddingBottom: "1em"
      }}
    >
      <List
        header="Bestehende Person verwenden"
        dataSource={existingUnregisteredUsers}
        renderItem={user => (
          <List.Item
            actions={[<a onClick={() => onPick(user)}>></a>]}
            onClick={() => onPick(user)}
            id="already-registered"
          >
            <List.Item.Meta
              title={`${user.firstname} ${user.lastname}`}
              avatar={
                <Avatar>
                  {user.firstname[0].toUpperCase()}
                  {user.lastname[0].toUpperCase()}
                </Avatar>
              }
            />
          </List.Item>
        )}
      />
      <style jsx global>{`
        #already-registered {
          padding-left: 8px;
        }
        #already-registered:hover {
          background-color: #f0f0f0;
          -webkit-transition: background-color 200ms linear;
          -ms-transition: background-color 200ms linear;
          transition: background-color 200ms linear;
        }
        #already-registered .ant-list-item-meta-content {
          flex: unset;
          align-self: center;
        }
        #already-registered .ant-list-item-meta-title {
          margin-bottom: 0px;
        }
      `}</style>
    </div>
  );
}
