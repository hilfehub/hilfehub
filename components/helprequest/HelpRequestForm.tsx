import { MultiStepForm } from "../MultiStepForm";
import {
  ShoppingCartOutlined,
  TeamOutlined,
  CarOutlined,
  EllipsisOutlined,
  UserOutlined,
  UserAddOutlined,
  LoadingOutlined
} from "@ant-design/icons";
import { useCallback, useState } from "react";
import { Addresses } from "../../backend";
import { VerticalButtonsStep } from "./VerticalButtonsStep";
import { LocationSelectionStep } from "./LocationSelectionStep";
import { TextAreaStep } from "./TextAreaStep";
import { ContactAndAddressInformationStep } from "./ContactAndAddressInformationStep";
import { LoginSignUpStep } from "./LoginSignupStep";
import {
  useActionButtonContextInvisible,
  useActionButtonContextWithStyle,
  useActionButtonContextDisableSetter,
  useActionButtonContextStyleSetter,
  useActionButtonContextClickSubscription,
  useActionButtonContextDisabledIf,
  useActionButtonBackInvisible
} from "../ActionButtonContext";
import { Spin } from "antd";
import { useEffectOnce } from "react-use";
import { EndStep } from "./EndStep";
import { useUserData, useLoginState } from "../TokenContext";
import { PhoneNumberStep } from "./PhoneNumberStep";
import { StepLayout } from "./StepLayout";
import { AddressForm } from "./AddressForm";
import { RegisteredUser, Task } from "../../types";

export type RequestType = Task["type"];
export type HelpSeekerType = "self" | "other";

export interface InputValue {
  type?: RequestType;
  shopMapboxId?: string;
  cart?: string;
  description?: string;
  furtherInformation?: string;
  helpSeeker?: HelpSeekerType;
  seekerId?: string;
  seekerPhone?: string;
  seekerFirstName?: string;
  seekerLastName?: string;
  seekerAddress?: Addresses.Create.Payload;
  creatorId?: string;
  creatorAddress?: Addresses.Create.Payload;
  transportSubject?: string;
  transportStart?: Addresses.Create.Payload;
  transportDestination?: Addresses.Create.Payload;
  phoneNumber?: string;
}

type StepNames =
  | "SetRequestType"
  | "SetErrandLocation"
  | "SetProductCart"
  | "SetFurtherInformation"
  | "SetHelpSeekerType"
  | "SetLoginData"
  | "Describe your task."
  | "Please describe the needed care."
  | "What needs to be transported?"
  | "What's the transport's start?"
  | "What's the transport's destination?"
  | "What's your phone number?"
  | "SetOtherContactInformation"
  | "What's your address?"
  | "End";

interface HelpRequestFormProps {
  header?: string;
  onExit: () => void;
  onDone: () => void;
}

function useActionButtonContextForNavigation(
  next: StepNames,
  goToStep: (step: StepNames) => void,
  goBack: () => void
) {
  const goNext = useCallback(() => goToStep(next), [goToStep, next]);

  useActionButtonContextClickSubscription(
    {
      actionButtonClicked: goNext,
      backButtonClicked: goBack
    },
    [goNext, goBack]
  );
}

export default function HelpRequestForm({
  header,
  onDone,
  onExit
}: HelpRequestFormProps) {
  return (
    <>
      <div id="pageWrapper">
        <h1 className="margin-top margins-sides">
          {header ?? "Anfrage stellen"}
        </h1>
        <MultiStepForm<InputValue, StepNames>
          initialStep="SetRequestType"
          initialInput={{}}
          steps={{
            SetRequestType: ({ goToStep, updateInput }) => {
              useActionButtonContextClickSubscription(
                {
                  actionButtonClicked: () => {},
                  backButtonClicked: onExit
                },
                [onExit]
              );

              useActionButtonContextInvisible();

              const nextStep = (stepName: StepNames, type: RequestType) => {
                updateInput("type", type);
                goToStep(stepName);
              };

              return (
                <VerticalButtonsStep
                  buttons={[
                    {
                      caption: "Besorgungen",
                      icon: <ShoppingCartOutlined />,
                      onClick: () => nextStep("SetErrandLocation", "errand")
                    },
                    {
                      caption: "Betreuung",
                      icon: <TeamOutlined />,
                      onClick: () =>
                        nextStep("Please describe the needed care.", "care")
                    },
                    {
                      caption: "Transport",
                      icon: <CarOutlined />,
                      onClick: () =>
                        nextStep("What needs to be transported?", "transport")
                    },
                    {
                      caption: "Sonstiges",
                      icon: <EllipsisOutlined />,
                      onClick: () => nextStep("Describe your task.", "plain")
                    }
                  ]}
                  title="Wofür möchtest du Hilfe anfragen?"
                />
              );
            },
            "Please describe the needed care.": ({
              goBack,
              goToStep,
              updateInput,
              currentInput
            }) => {
              useActionButtonContextForNavigation(
                "SetFurtherInformation",
                goToStep,
                goBack
              );

              useActionButtonContextWithStyle(
                {
                  caption: "Weiter",
                  disabled: true,
                  type: "primary"
                },
                []
              );

              const { description } = currentInput;
              const isValid = !!description;

              useActionButtonContextDisabledIf(!isValid);

              return (
                <TextAreaStep
                  title="Um wen muss sich gekümmert werden?"
                  defaultValues={{text: description}}
                  onChange={t => updateInput("description", t)}
                />
              );
            },
            "What needs to be transported?": ({
              goToStep,
              goBack,
              currentInput,
              updateInput
            }) => {
              useActionButtonContextForNavigation(
                "What's the transport's start?",
                goToStep,
                goBack
              );

              useActionButtonContextWithStyle(
                {
                  disabled: true,
                  type: "primary",
                  caption: "Weiter"
                },
                []
              );

              const [
                isDisabled,
                setIsDisabled
              ] = useActionButtonContextDisableSetter(true);

              const { transportSubject } = currentInput;
              const isValid = !!transportSubject;

              useActionButtonContextDisabledIf(!isValid);

              return (
                <TextAreaStep
                  title="Was muss transportiert werden?"
                  placeholder="Aktenordner, Schrank, Toilettenpapiervorräte ..."
                  defaultValues={{text: currentInput.transportSubject}}
                  onChange={(input: string) => {
                    const isInputValid = !!input;
                    updateInput("transportSubject", input);
                    setIsDisabled(!isInputValid);
                  }}
                />
              );
            },
            "What's the transport's start?": ({
              goToStep,
              goBack,
              updateInput,
              currentInput
            }) => {
              useActionButtonContextForNavigation(
                "What's the transport's destination?",
                goToStep,
                goBack
              );

              const { transportStart } = currentInput;
              const isValid = !!transportStart;

              useActionButtonContextWithStyle(
                {
                  caption: "Weiter",
                  type: "primary",
                  disabled: !isValid
                },
                [isValid]
              );

              console.log(currentInput.transportStart?.streetAndNumber);

              return (
                <StepLayout title="Wo soll der Transport starten?">
                  <AddressForm
                    defaultValues={{
                      streetAndNumber: currentInput.transportStart?.streetAndNumber,
                      suffix: currentInput.transportStart?.suffix,
                      zipCode: currentInput.transportStart?.zipCode,
                      town: currentInput.transportStart?.town
                    }}
                    onChange={a =>
                      updateInput("transportStart", a ?? undefined)
                    }
                  />
                </StepLayout>
              );
            },
            "What's the transport's destination?": ({
              goToStep,
              goBack,
              updateInput,
              currentInput
            }) => {
              useActionButtonContextForNavigation(
                "SetFurtherInformation",
                goToStep,
                goBack
              );

              const { transportDestination } = currentInput;
              const isValid = !!transportDestination;

              useActionButtonContextWithStyle(
                {
                  caption: "Weiter",
                  type: "primary",
                  disabled: !isValid
                },
                [isValid]
              );

              return (
                <StepLayout title="Wohin soll transportiert werden?">
                  <AddressForm
                    defaultValues={{
                      streetAndNumber: currentInput.transportDestination?.streetAndNumber,
                      suffix: currentInput.transportDestination?.suffix,
                      zipCode: currentInput.transportDestination?.zipCode,
                      town: currentInput.transportDestination?.town
                    }}
                    onChange={a =>
                      updateInput("transportDestination", a ?? undefined)
                    }
                  />
                </StepLayout>
              );
            },
            "Describe your task.": ({
              goToStep, 
              goBack,
              currentInput,
              updateInput 
            }) => {
              useActionButtonContextForNavigation(
                "SetFurtherInformation",
                goToStep,
                goBack
              );

              useActionButtonContextWithStyle(
                {
                  disabled: true,
                  type: "primary",
                  caption: "Weiter"
                },
                []
              );

              const [
                isDisabled,
                setIsDisabled
              ] = useActionButtonContextDisableSetter(true);

              const { description } = currentInput;
              const isValid = !!description;

              useActionButtonContextDisabledIf(!isValid);

              return (
                <TextAreaStep
                  title="Wobei brauchtst du Hilfe?"
                  additionalInfo="Bitte beschreibe, wobei genau du Hilfe benötigst."
                  placeholder={"Ich benötige Hilfe bei ..."}
                  defaultValues={{text: currentInput.description}}
                  onChange={(input: string) => {
                    const isInputValid = !!input;
                    updateInput("description", input);
                    setIsDisabled(!isInputValid);
                  }}
                />
              );
            },
            SetProductCart: ({
              goToStep, 
              updateInput, 
              goBack,
              currentInput
            }) => {
              useActionButtonContextForNavigation(
                "SetFurtherInformation",
                goToStep,
                goBack
              );

              useActionButtonContextWithStyle(
                {
                  caption: "Weiter",
                  disabled: true,
                  type: "primary"
                },
                []
              );

              const [
                isDisabled,
                setIsDisabled
              ] = useActionButtonContextDisableSetter(true);

              const { cart } = currentInput;
              const isValid = !!cart;

              useActionButtonContextDisabledIf(!isValid);

              return (
                <TextAreaStep
                  title="Was soll besorgt werden?"
                  additionalInfo="Je genauer, umso besser: Anzahl und Marke nicht vergessen. Pro Zeile
                    bitte nur ein Produkt aufschreiben."
                  placeholder={`Einkaufsliste, z. B.:\n1x Butter von Gut&Günstig\n2x Äpfel "Elstar"\n...`}
                  defaultValues={{text: currentInput.cart}}
                  onChange={(input: string) => {
                    const isInputValid = input.length > 4;
                    updateInput("cart", input);
                    setIsDisabled(!isInputValid);
                  }}
                />
              );
            },
            SetErrandLocation: ({
              goToStep,
              updateInput,
              goBack,
              currentInput
            }) => {
              useActionButtonContextWithStyle(
                {
                  caption: "Ort wählen",
                  disabled: false,
                  type: "invisible"
                },
                []
              );

              useActionButtonContextClickSubscription(
                {
                  actionButtonClicked: () => {},
                  backButtonClicked: goBack
                },
                []
              );

              return (
                <LocationSelectionStep
                  title="Wo können die Besorgungen erledigt werden?"
                  searchBarPlaceholder="z.B. Kaufland"
                  locationTypes={["poi", "address"]}
                  defaultValues={{text: ""}}
                  onLocationSelected={(location: Addresses.Create.Payload) => {
                    updateInput("shopMapboxId", location.mapboxId!);
                    goToStep("SetProductCart");
                  }}
                />
              );
            },
            SetFurtherInformation: ({
              goToStep,
              updateInput,
              goBack,
              currentInput
            }) => {
              useActionButtonContextForNavigation(
                "SetHelpSeekerType",
                goToStep,
                goBack
              );

              useActionButtonContextWithStyle(
                {
                  caption: !currentInput.furtherInformation ? "Überspringen" : "Weiter",
                  disabled: false,
                  type: !currentInput.furtherInformation ? "default" : "primary"
                },
                []
              );

              const setActionButtonStyle = useActionButtonContextStyleSetter();

              return (
                <TextAreaStep
                  title="Weitere Bemerkungen"
                  additionalInfo="Die folgenden Bemerkungen werden öffentlich einsehbar sein. Bitte
                    achte darauf, welche Informationen du preisgibst."
                  placeholder={`Weitere Bemerkungen, z.B.:\n- Wie dringend ist die Hilfe?\n- Möchtest du im Vorfeld vom Helfenden kontaktiert werden?\n- ggf. Hinweise zu Finanzen\n- ...`}
                  defaultValues={{text: currentInput.furtherInformation}}
                  onChange={(input: string) => {
                    const skipStep = input.length === 0;
                    updateInput("furtherInformation", input);
                    setActionButtonStyle(oldValues => ({
                      ...oldValues,
                      caption: skipStep ? "Überspringen" : "Weiter",
                      type: skipStep ? "default" : "primary"
                    }));
                  }}
                />
              );
            },
            SetHelpSeekerType: ({ goToStep, updateInput, goBack }) => {
              const nextStep = (stepName: StepNames, type: HelpSeekerType) => {
                updateInput("helpSeeker", type);
                goToStep(stepName);
              };

              useActionButtonContextForNavigation(
                "you won't get here" as any,
                goToStep,
                goBack
              );

              useActionButtonContextInvisible();

              return (
                <VerticalButtonsStep
                  buttons={[
                    {
                      caption: "Mich selbst",
                      icon: <UserOutlined />,
                      onClick: () => nextStep("SetLoginData", "self")
                    },
                    {
                      caption: "Jemand anderes",
                      icon: <UserAddOutlined />,
                      onClick: () =>
                        nextStep("SetOtherContactInformation", "other")
                    }
                  ]}
                  title="Für wen fragst du die Hilfe an?"
                />
              );
            },
            SetOtherContactInformation: ({ goToStep, updateInput, goBack }) => {
              useActionButtonContextForNavigation(
                "SetLoginData",
                goToStep,
                goBack
              );

              useActionButtonContextWithStyle(
                {
                  disabled: true,
                  type: "primary",
                  caption: "Weiter"
                },
                []
              );

              const [
                isDisabled,
                setDisabled
              ] = useActionButtonContextDisableSetter(true);

              return (
                <ContactAndAddressInformationStep
                  title="Über die hilfesuchende Person"
                  additionalInfo="Die folgenden Informationen werden nur der helfenden Person angezeigt.
                    Sie werden zur Kontaktaufnahme und Aufgabenerfüllung benötigt."
                  onPickExistingUser={user => {
                    updateInput("seekerId", user._id);
                    goToStep("SetLoginData");
                  }}
                  onChange={contactAndAddressInformation => {
                    if (!contactAndAddressInformation) {
                      setDisabled(true);
                      return;
                    }

                    setDisabled(false);

                    const {
                      address,
                      phone,
                      lastname,
                      firstname
                    } = contactAndAddressInformation;
                    updateInput("seekerFirstName", firstname);
                    updateInput("seekerLastName", lastname);
                    updateInput("seekerAddress", address);
                    updateInput("seekerPhone", phone);
                  }}
                />
              );
            },
            SetLoginData: ({ goToStep, updateInput, goBack, currentInput }) => {
              const next = useCallback(
                (userData: RegisteredUser) => {
                  const hasAddress = !!userData.address;
                  const hasPhoneNumber = !!userData.telephoneNumber;

                  const searchingHelpForThemselves =
                    currentInput.helpSeeker === "self";

                  if (!hasAddress && searchingHelpForThemselves) {
                    goToStep("What's your address?");
                    return;
                  }

                  if (!hasPhoneNumber) {
                    goToStep("What's your phone number?");
                    return;
                  }

                  goToStep("End");
                },
                [goToStep]
              );

              const user = useUserData();
              const isLoggedIn = useLoginState();

              useEffectOnce(() => {
                if (isLoggedIn) {
                  next(user!);
                }
              });

              const [
                isDisabled,
                setIsDisabled
              ] = useActionButtonContextDisableSetter(true);

              const [currentPane, setCurrentPane] = useState<
                "login" | "signup"
              >("signup");

              useActionButtonContextWithStyle(
                {
                  caption:
                    currentPane === "login" ? "Anmelden" : "Registrieren",
                  disabled: true,
                  type: "primary"
                },
                [currentPane]
              );

              useActionButtonContextClickSubscription(
                {
                  actionButtonClicked: () => {},
                  backButtonClicked: goBack
                },
                [goBack]
              );

              const handleValidationUpdate = useCallback(
                (inputIsValid: boolean) => {
                  setIsDisabled(!inputIsValid);
                },
                [setIsDisabled]
              );

              const handleLoginSuccessful = useCallback(
                (user: RegisteredUser) => {
                  updateInput("creatorId", user._id);
                  next(user);
                },
                [updateInput, goToStep, next]
              );

              const handleSignupSuccessful = useCallback(
                (user: RegisteredUser) => {
                  const { _id } = user;
                  updateInput("creatorId", _id);

                  next(user);
                },
                [updateInput, next]
              );

              const setActionButton = useActionButtonContextStyleSetter();

              const showSpinner = useCallback(
                (text: string) => {
                  setActionButton(old => ({
                    ...old,
                    caption: (
                      <Spin
                        delay={500}
                        indicator={
                          <>
                            <LoadingOutlined />
                            &nbsp;&nbsp;{text}
                          </>
                        }
                      />
                    ),
                    disabled: true
                  }));
                },
                [setActionButton]
              );

              const handleLoginLoading = useCallback(
                () => showSpinner("Anmelden..."),
                [showSpinner]
              );

              const handleSignupLoading = useCallback(
                () => showSpinner("Registrieren..."),
                [showSpinner]
              );

              const handleComplete = useCallback(
                () => setActionButton({ caption: "Weiter" }),
                [setActionButton]
              );

              return (
                <LoginSignUpStep
                  title="Melde dich an, um fortzufahren"
                  defaultKey="signup"
                  onLoginSuccessful={handleLoginSuccessful}
                  onSignupSuccessful={handleSignupSuccessful}
                  onValidationUpdate={handleValidationUpdate}
                  onLoginComplete={handleComplete}
                  onSignupComplete={handleComplete}
                  onLoginLoading={handleLoginLoading}
                  onSignupLoading={handleSignupLoading}
                  onPaneChanged={setCurrentPane}
                />
              );
            },
            "What's your address?": ({
              goToStep,
              goBack,
              currentInput,
              updateInput
            }) => {
              const userData = useUserData();

              const next = useCallback(() => {
                const hasPhoneNumber = !!userData?.telephoneNumber;
                if (!hasPhoneNumber) {
                  goToStep("What's your phone number?");
                } else {
                  goToStep("End");
                }
              }, [currentInput.phoneNumber, goToStep]);

              useActionButtonContextClickSubscription(
                {
                  actionButtonClicked: next,
                  backButtonClicked: goBack
                },
                [next, goBack]
              );

              const { creatorAddress } = currentInput;

              const isValid = !!creatorAddress;

              useActionButtonContextWithStyle(
                {
                  caption: "Weiter",
                  disabled: !isValid,
                  type: "primary"
                },
                [isValid]
              );

              return (
                <StepLayout
                  title="Verrat' uns bitte noch deine Addresse."
                  additionalInfo="Damit die Helfer auch wissen, wo sie helfen müssen."
                >
                  <AddressForm
                    defaultValues={{
                      streetAndNumber: currentInput.creatorAddress?.streetAndNumber,
                      suffix: currentInput.creatorAddress?.suffix,
                      zipCode: currentInput.creatorAddress?.zipCode,
                      town: currentInput.creatorAddress?.town
                    }}
                    onChange={a =>
                      updateInput("creatorAddress", a ?? undefined)
                    }
                  />
                </StepLayout>
              );
            },
            "What's your phone number?": ({
              goToStep,
              goBack,
              updateInput,
              currentInput
            }) => {
              const { phoneNumber } = currentInput;
              const [isValid, setIsValid] = useState(false);

              useActionButtonContextWithStyle(
                {
                  disabled: !isValid,
                  type: "primary",
                  caption: "Weiter"
                },
                [isValid]
              );

              useActionButtonContextForNavigation("End", goToStep, goBack);

              return (
                <PhoneNumberStep
                  value={phoneNumber || ""}
                  onChangeIsValid={setIsValid}
                  title="Bitte verrate uns noch deine Telefonnummer."
                  additionalInfo="Damit dich dein Helfer auch erreichen kann."
                  onChange={number => updateInput("phoneNumber", number)}
                />
              );
            },
            End: ({ goToStep, currentInput, goBack }) => {
              const [loading, setLoading] = useState(false);

              useActionButtonBackInvisible();

              useActionButtonContextClickSubscription(
                {
                  actionButtonClicked: onDone,
                  backButtonClicked: goBack
                },
                [goBack, onDone]
              );

              useActionButtonContextWithStyle(
                {
                  caption: "Weiter",
                  disabled: loading,
                  type: "primary"
                },
                [loading]
              );

              return (
                <EndStep
                  taskToCreate={currentInput}
                  onLoadingChange={setLoading}
                  onCreateAnotherTask={() => {
                    goToStep("SetRequestType");
                  }}
                />
              );
            }
          }}
        />
      </div>
      <style jsx>{`
        #pageWrapper {
          display: flex;
          height: 100%;
          flex-direction: column;
        }
      `}</style>
    </>
  );
}
