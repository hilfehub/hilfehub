import { StepLayoutProps, StepLayout } from "./StepLayout";
import { PhoneNumberInput } from "../PhoneNumberInput";

interface PhoneNumberStepProps extends StepLayoutProps {
  value: string;
  onChange: (number: string) => void;
  onChangeIsValid: (isValid: boolean) => void;
}

export function PhoneNumberStep(props: PhoneNumberStepProps) {
  const { value, onChange, onChangeIsValid, ...stepLayoutProps } = props;

  return (
    <StepLayout {...stepLayoutProps}>
      <PhoneNumberInput
        country="de"
        preferredCountries={["de"]}
        placeholder="Telefonnummer"
        onChange={onChange}
        onChangeIsValid={onChangeIsValid}
        value={value}
      />
    </StepLayout>
  );
}
