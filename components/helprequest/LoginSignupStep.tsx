import { StepLayoutProps, StepLayout } from "./StepLayout";
import { LoginSignupPage } from "../LoginSignupPage";
import { useCallback } from "react";
import { RegisteredUser } from "../../types";

interface LoginSignupStepProps extends StepLayoutProps {
  defaultKey?: "signup" | "login";
  onLoginLoading: () => void;
  onLoginComplete: () => void;
  onLoginSuccessful: (user: RegisteredUser) => void;
  onSignupLoading: () => void;
  onSignupComplete: () => void;
  onSignupSuccessful: (user: RegisteredUser) => void;
  onValidationUpdate: (isInputValid: boolean) => void;
  onPaneChanged?: (pane: "signup" | "login") => void;
}

export function LoginSignUpStep({
  defaultKey = "signup",
  onValidationUpdate,
  onLoginSuccessful,
  onSignupSuccessful,
  onLoginComplete,
  onLoginLoading,
  onSignupComplete,
  onSignupLoading,
  onPaneChanged,
  ...stepProps
}: LoginSignupStepProps) {
  const handleInputInvalid = useCallback(() => onValidationUpdate(false), [
    onValidationUpdate
  ]);
  const handleInputValid = useCallback(() => onValidationUpdate(true), [
    onValidationUpdate
  ]);

  return (
    <StepLayout {...stepProps}>
      <LoginSignupPage
        firstPane={defaultKey}
        onLoginSuccessful={onLoginSuccessful}
        onSignupSucessful={onSignupSuccessful}
        onInputInvalid={handleInputInvalid}
        onInputValid={handleInputValid}
        onLoginComplete={onLoginComplete}
        onLoginLoading={onLoginLoading}
        onSignupComplete={onSignupComplete}
        onSignupLoading={onSignupLoading}
        onPaneChanged={onPaneChanged}
      />
    </StepLayout>
  );
}
