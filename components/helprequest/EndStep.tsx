import { StepLayout } from "./StepLayout";
import { Result, Button, Spin } from "antd";
import { useState, useEffect } from "react";
import { useEffectOnce } from "react-use";
import { InputValue } from "./HelpRequestForm";
import { AxiosInstance } from "axios";
import { Addresses } from "../../backend";
import { PostAddressResponse } from "../../pages/api/addresses";
import { Users } from "../../backend";
import { CreateTaskPayload } from "../../pages/api/tasks";
import { useAxios } from "../useAPI";
import { usePhoneNumberSetter, useAddressSetter } from "../TokenContext";
import { UnregisteredUser, Address } from "../../types";

async function createTask(
  input: InputValue,
  axios: AxiosInstance,
  setPhoneNumber: (phoneNumber: string) => void,
  setAddress: (address: Address) => void
) {
  if (input.phoneNumber) {
    await axios.put("/users/self/telephoneNumber", input.phoneNumber!, {
      headers: { "Content-Type": "text/plain" }
    });
    setPhoneNumber(input.phoneNumber);
  }

  if (input.creatorAddress) {
    const { data } = await axios.put<Address>(
      "/users/self/address",
      input.creatorAddress
    );
    setAddress(data);
  }

  async function createAdress(
    address: Addresses.Create.Payload
  ): Promise<string> {
    const {
      data: { _id }
    } = await axios.post<PostAddressResponse>("/addresses", address);
    return _id;
  }

  async function createUnregisteredUser(
    address: Addresses.Create.Payload,
    firstname: string,
    lastname: string,
    phone: string
  ): Promise<string> {
    const user: Users.Create.UnregisteredUser.Payload = {
      firstname,
      isQuarantined: false,
      lastname,
      telephoneNumber: phone,
      address: address as any
    };
    const {
      data: { _id }
    } = await axios.post<UnregisteredUser>("/users/unregistered", user);
    return _id;
  }

  const startIdPromise = !!input.transportStart
    ? createAdress(input.transportStart)
    : Promise.resolve(undefined);
  const destinationIdPromise = !!input.transportDestination
    ? createAdress(input.transportDestination)
    : Promise.resolve(undefined);
  const clientIdPromise =
    input.helpSeeker === "other"
      ? !!input.seekerId
        ? Promise.resolve(input.seekerId)
        : createUnregisteredUser(
            input.seekerAddress!,
            input.seekerFirstName!,
            input.seekerLastName!,
            input.seekerPhone!
          )
      : Promise.resolve(undefined);

  const [startId, destinationId, clientId] = await Promise.all([
    startIdPromise,
    destinationIdPromise,
    clientIdPromise
  ]);

  const task: CreateTaskPayload = {
    type: input.type!,
    cart: input.cart,
    description: input.description,
    destinationId,
    clientId,
    furtherInformation: input.furtherInformation,
    shopMapboxId: input.shopMapboxId,
    startId,
    subject: input.transportSubject
  };

  await axios.post("/tasks", task);
}

interface EndStepProps {
  taskToCreate: InputValue;
  onCreateAnotherTask: () => void;
  onLoadingChange: (loading: boolean) => void;
}

export function EndStep(props: EndStepProps) {
  const { taskToCreate, onCreateAnotherTask, onLoadingChange } = props;

  const axios = useAxios();
  const setPhoneNumber = usePhoneNumberSetter();
  const setAddress = useAddressSetter();

  const [wasSuccessful, setWasSuccessful] = useState<boolean>();
  const loading = wasSuccessful === undefined;

  useEffect(() => {
    onLoadingChange(loading);
  }, [loading, onLoadingChange]);

  useEffectOnce(() => {
    async function doIt() {
      try {
        await createTask(taskToCreate, axios, setPhoneNumber, setAddress);
        setWasSuccessful(true);
      } catch (error) {
        setWasSuccessful(false);
      }
    }

    doIt();
  });

  if (loading) {
    return (
      <StepLayout>
        <Spin />
      </StepLayout>
    );
  }

  if (wasSuccessful) {
    return (
      <StepLayout>
        <Result
          status="success"
          title="Deine Aufgabe wurde erfolgreich erstellt."
          subTitle={[]}
          extra={[
            <Button onClick={onCreateAnotherTask}>
              Neue Aufgabe erstellen
            </Button>
          ]}
        />
      </StepLayout>
    );
  } else {
    return (
      <StepLayout>
        <Result
          status="error"
          title="Etwas ist schiefgelaufen"
          subTitle="Das tut uns Leid."
          extra={[
            <Button onClick={onCreateAnotherTask}>
              Neue Aufgabe erstellen
            </Button>
          ]}
        />
      </StepLayout>
    );
  }
}
