import { Form, AutoComplete, Input, InputNumber } from "antd";
import { Addresses } from "../../backend";
import { useState, useEffect, useRef } from "react";
import { findLocationByIp } from "../findLocationByIP";
import { useEffectOnce } from "react-use";
import { useDebouncedMapboxSearch } from "../useMapboxSearch";
import { isFormValid } from "../SignUpForm";

interface StreetInputProps {
  onChange?: (streetAndNumber: string) => void;
  value?: string;
  onSelectAddress: (address: Addresses.Create.Payload) => void;
  placeholder?: string;
  defaultValue?: string;
}

interface StreetInputSearchResult {
  value: string;
  mapboxId: string;
  position: [number, number];
}

function StreetInput(props: StreetInputProps) {
  const { onChange = () => {}, value, onSelectAddress, placeholder, defaultValue } = props;

  const [ipLocation, setIpLocation] = useState<[number, number]>();
  useEffectOnce(() => {
    async function doIt() {
      const { longitude, latitude } = await findLocationByIp();
      setIpLocation([longitude, latitude]);
    }

    doIt();
  });

  const [
    mapboxResults,
    handleSearch,
    isLoadingMapboxResults
  ] = useDebouncedMapboxSearch({ types: ["address"] }, 500);

  useEffect(() => {
    handleSearch(value ?? "", ipLocation);
  }, [ipLocation, value, handleSearch]);

  const options = mapboxResults.map(feature => ({
    value: feature.place_name,
    mapboxId: feature.id,
    position: feature.center
  }));

  return (
    <AutoComplete
      options={options}
      value={value}
      placeholder={placeholder}
      defaultValue={defaultValue}
      onChange={onChange}
      onSelect={(term, option) => {
        const { value, mapboxId, position } = option as StreetInputSearchResult;
        const [streetAndNumber, place, nation] = (value as string).split(", ");
        const [zipCode, town] = place.split(" ");
        onChange(streetAndNumber);
        onSelectAddress({
          streetAndNumber,
          town,
          zipCode,
          suffix: "",
          mapboxId,
          position
        });
      }}
    >
      <Input.Search
        loading={isLoadingMapboxResults}
      />
    </AutoComplete>
  );
}

interface AddressFormProps {
  defaultValues?: {
    streetAndNumber?: string;
    zipCode?: string;
    town?: string;
    suffix?: string;
  };
  onChange?: (address: Addresses.Create.Payload | null) => void;
}

export function AddressForm(props: AddressFormProps) {
  const { onChange = () => {}, defaultValues } = props;

  const [form] = Form.useForm();

  return (
    <Form
      form={form}
      onValuesChange={async () => {
        const formIsValid = await isFormValid(form);
        if (!formIsValid) {
          onChange(null);
          return;
        }

        const {
          streetAndNumber,
          suffix,
          zipCode,
          town,
          mapboxId,
          position
        } = form.getFieldsValue();
        const allNeededPresent = !!streetAndNumber && !!zipCode && !!town;
        if (!allNeededPresent) {
          onChange(null);
        } else {
          onChange({
            streetAndNumber,
            town,
            zipCode,
            mapboxId,
            position,
            suffix: suffix ?? ""
          });
        }
      }}
    >
      <Form.Item
        name="streetAndNumber"
        rules={[
          {
            required: true,
            message: "Bitte geben Sie Straße und Hausnummer ein."
          }
        ]}
      >
        <StreetInput
          onSelectAddress={address => {
            const { zipCode, town, position, mapboxId } = address;
            form.setFields([
              { name: "mapboxId", value: mapboxId },
              { name: "position", value: position },
              { name: "zipCode", value: zipCode },
              { name: "town", value: town }
            ]);
          }}
          placeholder="Straße und Hausnummer"
          defaultValue={defaultValues?.streetAndNumber}
        />
      </Form.Item>

      <Form.Item name="suffix">
        <Input
          placeholder="Zusatz"
          defaultValue={defaultValues?.suffix}  
        />
      </Form.Item>

      <Form.Item
        name="zipCode"
        rules={[
          {
            required: true,
            message: "Bitte geben Sie eine Postleitzahl an."
          }
        ]}
      >
        <Input
          placeholder="Postleitzahl"
          defaultValue={defaultValues?.zipCode}
        />
      </Form.Item>

      <Form.Item
        name="town"
        rules={[
          {
            required: true,
            message: "Bitte geben Sie eine Stadt an."
          }
        ]}
      >
        <Input
          placeholder="Stadt"
          defaultValue={defaultValues?.town}
        />
      </Form.Item>

      <Form.Item name="mapboxId" noStyle>
        <Input hidden />
      </Form.Item>

      <Form.Item name="position" noStyle>
        <Input hidden />
      </Form.Item>
    </Form>
  );
}
