import { PropsWithChildren } from "react";
import clsx from "clsx";

export interface StepLayoutProps {
  className?: string;
  title?: string;
  additionalInfo?: string;
  bottomMargin?: boolean;
  sideMargins?: boolean;
  defaultValues?: object;
}

export function StepLayout({
  className,
  title,
  additionalInfo,
  bottomMargin = true,
  sideMargins = true,
  children
}: PropsWithChildren<StepLayoutProps>) {
  return (
    <div className={clsx("step", className)}>
      {!!title && <h3 className="margins-sides">{title}</h3>}
      {!!additionalInfo && (
        <div
          className={clsx(
            "margins-sides",
            "additionalInfo",
            !title && "margin-top"
          )}
        >
          {additionalInfo}
        </div>
      )}
      <div className={clsx("flex-vertical", sideMargins && "margins-sides")}>
        <div className={clsx("flex-vertical", bottomMargin && "margin-bottom")}>
          {children}
        </div>
      </div>
      <style jsx>{`
        .additionalInfo {
          margin-bottom: 0.5rem;
        }
        .additionalInfo.margin-top {
          margin-top: 0.5rem;
        }
        .flex-vertical {
          display: flex;
          flex-direction: column;
          flex: 1;
        }
      `}</style>
    </div>
  );
}
