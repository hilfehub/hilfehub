import { StepLayoutProps, StepLayout } from "./StepLayout";
import { Addresses } from "../../backend";
import { ConfiguredMap, MarkerWithClickHandler } from "../ConfiguredMap";
import { Input } from "antd";
import {
  useDebouncedMapboxSearch,
  mapboxGeocodingFeatureToAddress,
  MapboxSearchType
} from "../useMapboxSearch";
import { useState, useEffect } from "react";
import { ViewportProps } from "react-map-gl";

interface LocationSelectionStepProps extends StepLayoutProps {
  locationTypes: MapboxSearchType[];
  searchBarPlaceholder?: string;
  onLocationSelected?: (location: Addresses.Create.Payload) => void;
  defaultValues?: {text?: string}
}

export function LocationSelectionStep({
  searchBarPlaceholder,
  locationTypes,
  onLocationSelected = () => {},
  defaultValues = {text: ""},
  ...stepProps
}: LocationSelectionStepProps) {
  const [results, updateSearch, isLoading] = useDebouncedMapboxSearch(
    { types: locationTypes, limit: 10 },
    500
  );

  const [searchTerm, setSearchTerm] = useState<string>("");
  const [viewport, setViewport] = useState<ViewportProps>();
  const { latitude, longitude } = viewport || {};

  useEffect(() => {
    const location: [number, number] | undefined =
      longitude && latitude ? [longitude, latitude] : undefined;
    updateSearch(searchTerm, location);
  }, [updateSearch, latitude, longitude, searchTerm]);

  return (
    <StepLayout {...stepProps} bottomMargin={false} sideMargins={false}>
      <div id="pageSpacer">
        <div id="mapWrapper">
          <ConfiguredMap onChangeViewPort={setViewport}>
            {results.map(result => {
              const {
                center: [longitude, latitude],
                id,
                place_name
              } = result;
              return (
                <MarkerWithClickHandler
                  key={id}
                  latitude={latitude}
                  longitude={longitude}
                  onClick={() => {
                    const address = mapboxGeocodingFeatureToAddress(result);
                    onLocationSelected(address);
                  }}
                >
                  {place_name}
                </MarkerWithClickHandler>
              );
            })}
          </ConfiguredMap>
          <div id="searchBar">
            <Input.Search
              value={searchTerm}
              size="large"
              placeholder={searchBarPlaceholder}
              style={{ width: "100%" }}
              loading={isLoading}
              defaultValue={defaultValues.text}
              onChange={evt => {
                setSearchTerm(evt.target.value);
              }}
            />
          </div>
        </div>
      </div>
      <style jsx>{`
        #pageSpacer {
          display: flex;
          height: 100%;
          flex-direction: column;
        }
        #mapWrapper {
          margin-top: 1rem;
          flex: 1;
          position: relative;
          overflow: hidden;
        }
        #mapWrapper > #searchBar {
          position: absolute;
          width: auto;
          left: 2rem;
          right: 2rem;
          bottom: 1.5rem;
        }
      `}</style>
    </StepLayout>
  );
}
