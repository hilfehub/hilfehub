import clsx from "clsx";

interface LayoutProps {
  fullScreenPage?: boolean;
  menu: React.ReactChild;
}

export default function MenuLayout({
  children,
  menu,
  fullScreenPage = false
}: React.PropsWithChildren<LayoutProps>) {
  return (
    <div className="wrapper">
      <header>
        <img id="bg" src="/graphics/landing_page_bottom_bg.png" />
        <img id="logo" src="/graphics/logo_word_image_white_beta_min.svg" />
      </header>
      <div className={clsx("page", !fullScreenPage && "extra-margins-sides")}>
        {children}
      </div>
      {menu}
      <style jsx>{`
        .wrapper {
          height: 100%;
          display: grid;
          grid-template-columns: 1fr;
          grid-template-rows: 0 10fr 1fr;
          grid-template-areas:
            "header"
            "page"
            "menu";
          overflow: hidden;
        }
        .page {
          grid-area: page;
          overflow: auto;
          position: relative;
          z-index: 0;
        }
        header {
          grid-area: header;
          overflow: hidden;
          position: relative;
        }
        header > #bg {
          width: 100%;
          transform: rotate(180deg);
        }
        header > #logo {
          position: absolute;
          height: 60%;
          top: 50%;
          left: 50%;
          transform: translate(-50%, -50%);
        }

        @media (min-height: 700px) {
          .wrapper {
            grid-template-rows: 5rem auto 5rem;
          }
        }

        @media (min-height: 900px) {
          .wrapper {
            grid-template-areas:
              "header"
              "page"
              "menu";
          }
        }
      `}</style>
    </div>
  );
}
