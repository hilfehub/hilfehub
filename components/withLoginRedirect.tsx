import { useRouter } from "next/router";
import { useLoginState } from "./TokenContext";
import { useEffect } from "react";

export function withLoginRedirect<T>(Component: React.ComponentType<T>) {
  return function WithLoginRedirect(props: T) {
    const router = useRouter();
    const isLoggedIn = useLoginState();

    useEffect(() => {
      if (!isLoggedIn) {
        router.push(
          { pathname: "/login", query: { goBackOnDone: true } },
          "/login"
        );
      }
    }, [isLoggedIn, router]);

    return <Component {...props} />;
  };
}
