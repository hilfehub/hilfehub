import {
  mapboxGeocodingFeatureToAddress,
  MapboxGeocodingFeature
} from "./useMapboxSearch";
import { Addresses } from "../backend";

describe("mapboxGeocodingFeatureToAddress", () => {
  it("maps POI to its Address", () => {
    const edekaSchüren: MapboxGeocodingFeature = {
      id: "poi.317827655517",
      type: "Feature",
      place_type: ["poi"],
      relevance: 1,
      properties: { landmark: true, address: "Röttgener Str. 69" },
      text: "EDEKA Schüren",
      place_name:
        "EDEKA Schüren, Röttgener Str. 69, Bonn, Nordrhein-Westfalen 53127, Germany",
      center: [7.08022, 50.700312],
      geometry: { coordinates: [7.08022, 50.700312], type: "Point" },
      context: [
        {
          id: "locality.2222355553155690",
          wikidata: "Q880975",
          text: "Ippendorf"
        }
      ]
    };

    const address = mapboxGeocodingFeatureToAddress(edekaSchüren);

    const expected: Addresses.Create.Payload = {
      mapboxId: "poi.317827655517",
      streetAndNumber: "Röttgener Str. 69",
      suffix: "",
      town: "Bonn",
      zipCode: "53127",
      name: "EDEKA Schüren",
      position: [7.08022, 50.700312]
    };

    expect(address).toEqual(expected);
  });
});
