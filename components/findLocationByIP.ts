import Axios from "axios";

interface IPInfoResponse {
  ip: string;
  hostname: string;
  city: string;
  region: string;
  country: string;
  loc: string;
  org: string;
  postal: string;
  timezone: string;
  readme: string;
}

export async function findLocationByIp(): Promise<{
  latitude: number;
  longitude: number;
}> {
  const response = await Axios.get<IPInfoResponse>("https://ipinfo.io/", {
    params: { token: process.env.IPINFO_TOKEN }
  });
  const {
    data: { loc }
  } = response;

  const [lat, lon] = loc.split(",");

  return {
    latitude: +lat,
    longitude: +lon
  };
}
