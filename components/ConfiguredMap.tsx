import AutoSizedMapboxMap from "./AutoSizedMapboxMap";
import { useState } from "react";
import { EnvironmentOutlined } from "@ant-design/icons";
import {
  ScaleControl,
  GeolocateControl,
  MarkerProps,
  ViewportProps,
  Marker
} from "react-map-gl";
import { useEffectOnce } from "react-use";
import { findLocationByIp } from "./findLocationByIP";
import clsx from "clsx";

interface MarkerWithClickHandlerProps extends MarkerProps {
  onClick?: () => void;
}

export function MarkerWithClickHandler(
  props: React.PropsWithChildren<MarkerWithClickHandlerProps>
) {
  const { onClick, className, ...rest } = props;
  return (
    <Marker
      {...rest}
      className={clsx("marker", className)}
      ref={marker => {
        if (marker && marker._containerRef.current && onClick) {
          marker._containerRef.current.onclick = onClick;
        }
      }}
    >
      <img src="/graphics/marker.svg" className="marker" />
      <style jsx>{`
        .marker {
          cursor: pointer;
          height: 1.3rem;
          transform: translate(-50%, -100%);
        }
      `}</style>
    </Marker>
  );
}

interface HashLocation {
  latitude: number;
  longitude: number;
  zoom: number;
}

function getMapHashLocation(): HashLocation | undefined {
  if (typeof window === "undefined") {
    return undefined;
  }

  const { hash } = location;
  const result = /#map=([\d|.]*)\/([\d|.]*)\/([\d|.]*)/.exec(hash);
  if (!result) {
    return undefined;
  }

  const [all, zoom, latitude, longitude] = result;

  return {
    zoom: +zoom,
    longitude: +longitude,
    latitude: +latitude
  };
}

interface ConfiguredMapProps {
  onChangeViewPort?: (vp: ViewportProps) => void;
}

export function ConfiguredMap(
  props: React.PropsWithChildren<ConfiguredMapProps>
) {
  const { children, onChangeViewPort = () => {} } = props;

  const initialLocation = getMapHashLocation();
  const [viewport, setViewport] = useState({
    latitude: 51.448,
    longitude: 9.761,
    zoom: 5.2,
    ...(initialLocation ?? {})
  });

  useEffectOnce(() => {
    if (!!initialLocation) {
      return;
    }

    async function doIt() {
      const location = await findLocationByIp();
      setViewport(vp => ({
        ...vp,
        ...location,
        zoom: 8
      }));
    }

    doIt();
  });

  return (
    <AutoSizedMapboxMap
      {...viewport}
      onViewportChange={vp => {
        onChangeViewPort(vp);
        setViewport(vp);
      }}
      mapStyle="mapbox://styles/mapbox/streets-v11?optimize=true"
      mapboxApiAccessToken={process.env.MAPBOXGL_TOKEN}
      mapOptions={{
        logoPosition: "top-left",
        hash: "map"
      }}
    >
      <div style={{ position: "absolute", bottom: 10, left: 10 }}>
        <ScaleControl maxWidth={150} unit="metric" />
      </div>

      <GeolocateControl
        style={{ position: "absolute", right: "10px", top: "10px" }}
        positionOptions={{ enableHighAccuracy: true }}
        trackUserLocation
      />

      {children}
    </AutoSizedMapboxMap>
  );
}
