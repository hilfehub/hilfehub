import { Input, notification, Form, Row, Col, Button } from "antd";
import { MailOutlined, LockOutlined } from "@ant-design/icons";
import { useCallback } from "react";
import axios from "axios";
import { InvisibleSubmitButton } from "./InvisibleSubmitButton";

async function invokePasswordReset(email: string): Promise<void> {
  await axios.post(`/api/users/byEmail/${email}/invokePasswordReset`);
}

interface LoginFormProps {
  onChange: (creds: [string, string]) => void;
  onSubmit?: () => void;
}

export default function LoginForm(props: LoginFormProps) {
  const { onChange, onSubmit } = props;

  const [form] = Form.useForm();

  const handlePasswordReset = useCallback(async () => {
    const email = form.getFieldValue("email");
    if (!email) {
      notification.warning({
        message: "E-Mail-Addresse fehlt",
        description:
          "Bitte geben Sie die E-Mail-Addresse ein, deren Passwort Sie vergessen haben."
      });
    }
    await invokePasswordReset(email);

    notification.open({
      message: "Rücksetzung des Passworts angefordert",
      description:
        "Sie erhalten in Kürze eine E-Mail mit den weiteren Schritten."
    });
  }, [form]);

  return (
    <Form
      form={form}
      name="login"
      className="login-form"
      onFinish={onSubmit}
      onValuesChange={() => {
        onChange([form.getFieldValue("email"), form.getFieldValue("password")]);
      }}
      size="large"
      style={{ paddingTop: "12px" }}
    >
      <Form.Item
        name="email"
        rules={[
          {
            required: true,
            message: "Bitte geben Sie eine E-Mail-Addresse an."
          }
        ]}
      >
        <Input
          prefix={<MailOutlined />}
          placeholder="E-Mail-Addresse"
          autoComplete="username"
        />
      </Form.Item>

      <Form.Item
        name="password"
        rules={[
          { required: true, message: "Bitte geben Sie ihr Passwort ein." }
        ]}
      >
        <Input.Password
          prefix={<LockOutlined />}
          type="password"
          placeholder="Passwort"
          autoComplete="current-password"
        />
      </Form.Item>

      <Button type="link" size="small" onClick={handlePasswordReset}>
        Passwort vergessen
      </Button>

      <InvisibleSubmitButton />
    </Form>
  );
}
