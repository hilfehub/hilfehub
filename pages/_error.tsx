import NextError, { ErrorProps } from "next/error";
import { NextPageContext } from "next";
import { Sentry } from "../components/Sentry";

interface MyErrorProps extends ErrorProps {
  hasGetInitialPropsRun?: boolean;
  err?: any;
}

function MyError(props: MyErrorProps) {
  const { statusCode, hasGetInitialPropsRun, err } = props;

  if (!hasGetInitialPropsRun && !!err) {
    Sentry.captureException(err);
  }

  return <NextError statusCode={statusCode} />;
}

MyError.getInitialProps = async (ctx: NextPageContext) => {
  const errorInitialProps: MyErrorProps = await NextError.getInitialProps(ctx);

  const { res, err, asPath } = ctx;
  errorInitialProps.hasGetInitialPropsRun = true;

  const isRunningOnServer = !!res;

  if (isRunningOnServer) {
    if (res?.statusCode === 404) {
      return { statusCode: 404 };
    }

    if (err) {
      Sentry.captureException(err, ctx);

      return errorInitialProps;
    }
  } else {
    if (err) {
      Sentry.captureException(err, ctx);
    }

    return errorInitialProps;
  }

  Sentry.captureException(
    new Error(`_error.tsx getInitialProps missing data at path: ${asPath}`)
  );

  return errorInitialProps;
};

export default MyError;
