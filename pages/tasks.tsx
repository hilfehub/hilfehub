import MenuLayout from "../components/MenuLayout";
import Menu from "../components/Menu";
import Item from "../components/ListItem";
import Divider from "../components/ListDivider";
import { usePickedTasks, useCreatedTasks } from "../components/useTasks";
import { Empty } from "antd";
import LoadingSpinner from "../components/LoadingSpinner";
import { withLoginRedirect } from "../components/withLoginRedirect";
import { getUserName } from "../components/TaskProcessors";
import { Task } from "../types";
import Head from "../components/Head";

interface TaskItemProps {
  task: Task;
}

export function taskName(task: Task): string {
  let result = "";
  switch (task.type) {
    case "errand":
      result += "Besorgung für ";
      break;
    case "care":
      result += "Betreuung für ";
      break;
    case "transport":
      result += "Transport für ";
      break;
    default:
      result += "Hilfe für ";
      break;
  }
  const { firstname } = task.client ?? task.creator;
  return result + firstname;
}

function taskDescription(task: Task): string {
  switch (task.type) {
    case "transport":
      if (task.start.town === task.start.town)
        return "Innerhalb von " + task.start.town;
      else return "Von " + task.start.town + " nach " + task.destination.town;
    case "errand":
    case "care":
    default:
      return (
        "Angefragt am " +
          new Date(task.placedAt).toLocaleDateString("de-DE", {
            day: "numeric",
            month: "long"
          }) ?? ""
      );
  }
}

function PickedTaskItem(props: TaskItemProps) {
  const { task } = props;

  return (
    <Item
      name={taskName(task)}
      caption={taskDescription(task)}
      href="/t/[taskid]"
      as={`/t/${task._id}`}
    />
  );
}

function createdTaskDescription(task: Task) {
  const { pickedBy } = task;

  if (pickedBy) {
    return `${getUserName(task.pickedBy) || "Eine Person"} hilft dir`;
  } else {
    return taskDescription(task);
  }
}

function CreatedTaskItem(props: TaskItemProps) {
  const { task } = props;

  return (
    <Item
      name={taskName(task)}
      caption={createdTaskDescription(task)}
      href="/t/[taskid]"
      as={`/t/${task._id}`}
    />
  );
}

interface TaskListProps {
  sectionTitle: string;
  tasks: Task[];
  Component: React.ComponentType<TaskItemProps>;
}

function TaskList(props: TaskListProps) {
  const { tasks, Component, sectionTitle } = props;

  if (tasks.length === 0) {
    return null;
  }

  return (
    <>
      <Divider title={sectionTitle} />
      {tasks.map(t => (
        <Component key={t._id} task={t} />
      ))}
    </>
  );
}

function Tasks() {
  const [allPickedTasks, pickedTasksAreLoading] = usePickedTasks();
  const [createdTasks, createdTasksAreLoading] = useCreatedTasks();

  const uncompletedPickedTasks = allPickedTasks.filter(t => !t.completed);

  const bothAreEmpty =
    uncompletedPickedTasks.length === 0 && createdTasks.length === 0;
  const oneIsLoading = pickedTasksAreLoading || createdTasksAreLoading;

  const showLoadingSpinner = bothAreEmpty && oneIsLoading;
  const nothingIsShown = bothAreEmpty && !oneIsLoading;

  return (
    <MenuLayout menu={<Menu active="tasks" />}>
      <Head title="Aufgabenübersicht" />
      <h1 className="margins-sides margin-top">Deine Aufgaben</h1>

      {nothingIsShown && (
        <Empty
          image={Empty.PRESENTED_IMAGE_SIMPLE}
          description="Hier ist noch nichts zu sehen. Öffne das Hilferadar, um Hilfe zu suchen oder Aufgaben anzunehmen."
        />
      )}

      <LoadingSpinner
        isLoading={showLoadingSpinner}
        loadingMessage="Aufgaben werden geladen ..."
      >
        <TaskList
          sectionTitle="Anstehende Aufgaben"
          tasks={uncompletedPickedTasks}
          Component={PickedTaskItem}
        />

        <TaskList
          sectionTitle="Erstellte Aufgaben"
          tasks={createdTasks}
          Component={CreatedTaskItem}
        />
      </LoadingSpinner>
    </MenuLayout>
  );
}

export default withLoginRedirect(Tasks);
