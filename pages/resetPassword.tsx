import { useRouter } from "next/router";
import { Form, Input, Button } from "antd";
import { useCallback } from "react";
import axios from "axios";
import { ResetPasswordResponse } from "./api/resetPassword/[token]";
import { useTokenSetter } from "../components/TokenContext";
import Head from "../components/Head";

export default function ResetPasswordPage() {
  const router = useRouter();
  const token = router.query.token as string;
  const setToken = useTokenSetter();

  const handleSubmit = useCallback(
    async inputs => {
      const password = inputs.password as string;

      const response = await axios.post<ResetPasswordResponse>(
        `/api/resetPassword/${token}`,
        password,
        { headers: { "Content-Type": "text/plain" } }
      );

      const {
        data: { token: newToken, user: newUser }
      } = response;
      setToken(newToken, newUser);

      await router.push("/radar");
    },
    [token, setToken, router]
  );

  return (
    <div>
      <Head title="Passwort zurücksetzen" />
      <p>Sie wollen ihr Passwort zurücksetzen. Können sie hier machen.</p>

      <Form onFinish={handleSubmit}>
        <Form.Item
          name="password"
          rules={[
            {
              required: true,
              message: "Bitte geben sie ihr neues Passwort ein."
            }
          ]}
        >
          <Input.Password
            size="large"
            placeholder="Neues Passwort"
            autoComplete="new-password"
          />
        </Form.Item>

        <Form.Item>
          <Button type="primary" htmlType="submit">
            Passwort zurücksetzen
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
