import { useRouter } from "next/router";
import Menu from "../../components/ActionButtonMenu";
import Layout from "../../components/MenuLayout";
import Head from "../../components/Head";
import {
  CreatorTaskView,
  PickerTaskView,
  DefaultTaskView
} from "../../components/TaskView";
import { Empty, Result } from "antd";
import { ReactChild } from "react";
import LoadingSpinner from "../../components/LoadingSpinner";
import { useUserData } from "../../components/TokenContext";
import {
  useTask,
  useTaskPicker,
  useTaskCompleter
} from "../../components/useTasks";

function Task() {
  const router = useRouter();
  const taskId = router.query.taskid as string;

  const [task, isLoading, reload] = useTask(taskId);

  const userId = useUserData()?._id;

  const pickTask = useTaskPicker();
  const completeTask = useTaskCompleter();

  const isTaskCreator = task?.creator._id === userId;
  const isTaskPicker = task?.pickedBy?._id === userId;

  if (!task && !isLoading) {
    return (
      <Layout menu={<Menu type="invisible" />}>
        <div className="centerVertically">
          <Empty
            image={Empty.PRESENTED_IMAGE_SIMPLE}
            description="Hier wird keine Hilfe benötigt :)"
          />
        </div>
        <style jsx>{`
          .centerVertically {
            height: 100%;
            display: flex;
            flex-direction: column;
            justify-content: center;
          }
        `}</style>
      </Layout>
    );
  }

  let menu: ReactChild = <Menu type="invisible" />;
  let view: ReactChild | null = null;

  if (task?.completed) {
    view = (
      <Result
        status="success"
        title="Diese Aufgabe wurde erledigt."
        subTitle="Toll 😄"
      />
    );
  } else if (isTaskCreator && !!task) {
    view = <CreatorTaskView task={task} onReload={reload} />;
  } else if (isTaskPicker) {
    menu = (
      <Menu
        caption="Aufgabe abgearbeitet"
        disabled={!task && isLoading}
        onClick={async () => {
          await completeTask(taskId);
          await reload();
        }}
      />
    );
    if (!!task) view = <PickerTaskView task={task} onReload={reload} />;
  } else if (!!task) {
    menu = (
      <Menu
        caption="Aufgabe annehmen"
        disabled={!task && isLoading}
        onClick={async () => {
          await pickTask(taskId);
          await reload();
        }}
      />
    );
    view = <DefaultTaskView task={task} onReload={reload} />;
  }

  return (
    <Layout menu={menu}>
      <Head noindex nofollow />
      <LoadingSpinner
        isLoading={!task && isLoading}
        loadingMessage="Aufgabe wird geladen ..."
      >
        {view}
      </LoadingSpinner>
    </Layout>
  );
}

export default Task;
