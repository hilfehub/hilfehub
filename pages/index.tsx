import { Button } from "antd";
import {
  AlertFilled,
  HeartFilled,
  CommentOutlined,
  SolutionOutlined,
  LinkOutlined
} from "@ant-design/icons";
import Link from "next/link";
import { useLoginState } from "../components/TokenContext";
import { useEffect } from "react";
import { useRouter } from "next/router";
import { useSessionStorage } from "react-use";
import Head from "../components/Head";

function Index() {
  const isLoggedIn = useLoginState();
  const router = useRouter();
  const [isFirstVisit, setIsFirstVisit] = useSessionStorage(
    "isFirstVisit",
    true
  );

  useEffect(() => {
    if (isLoggedIn && isFirstVisit) {
      setIsFirstVisit(false);
      router.replace("/radar");
    }
  }, [isLoggedIn, router, isFirstVisit, setIsFirstVisit]);

  return (
    <div id="page">
      <Head />
      <div id="topImageWrapper" className="extra-margins-sides">
        <img
          id="topBg"
          className="noselect"
          src="/graphics/landing_page_top_bg.png"
        />
        <img
          id="logo"
          className="noselect"
          src="/graphics/logo_word_image_white_beta_large_min.svg"
        />
      </div>
      <div
        className="pageWrapper extra-margins-sides"
        style={{
          flex: "1",
          alignItems: "center",
          padding: "0"
        }}
      >
        <div id="contentWrapper" className="margins-sides">
          <h2 className="centered">
            Vermittlungsplattform für Helfende und Hilfesuchende.
          </h2>
          <Link href="/request">
            <Button size="large" type="primary" icon={<AlertFilled />} block>
              Hilfe beantragen
            </Button>
          </Link>
          <Link
            href={{
              pathname: "/login",
              query: { type: "volunteer", action: "signup" }
            }}
          >
            <Button size="large" type="primary" icon={<HeartFilled />} block>
              Ich möchte helfen
            </Button>
          </Link>
          <div className="additional-section component-border">
            <div id="icon">
              <CommentOutlined />
            </div>
            <div className="contentWrapper">
              <div id="header">Wir sind zurzeit in der BETA-Phase...</div>
              <div id="description">
                ... und freuen uns deswegen besonders jetzt über Fehlermeldungen
                und Feedback! Du erreichst uns unkompliziert über folgende
                E-Mail:
                <br />
                <span id="mail">
                  incoming+hilfehub-hilfehub-17520557-issue-@incoming.gitlab.com
                </span>
              </div>
              <div id="button">
                <Button
                  block
                  href="mailto:incoming+hilfehub-hilfehub-17520557-issue-@incoming.gitlab.com?subject=HilfeHub%20Feedback&body=Liebes%20HilfeHub-Team%2C%0D%0A%0D%0A%3CHier%20Probleme%2C%20Fragen%2C%20Lob%20und%20Anmerkungen%20einf%C3%BCgen%3E%0D%0A%0D%0AViele%20Gr%C3%BC%C3%9Fe%2C%0D%0A%3CWie%20d%C3%BCrfen%20wir%20dich%20ansprechen%3F%3E"
                  size="large"
                >
                  Feedback schreiben
                </Button>
              </div>
            </div>
          </div>

          <div className="additional-section component-border filled">
            <div id="icon">
              <SolutionOutlined />
            </div>
            <div className="contentWrapper">
              <div id="header">Arbeitslos wegen Corona?</div>
              <div id="description">
                Unser Partner Andreas Blum sammelt Empfehlungen und
                Stellenanzeigen für alle, die in der letzten Zeit ihre Arbeit
                verloren haben.
              </div>
              <div id="button">
                <Button
                  block
                  href="https://blummedia.de/jobsuche-wahrend-der-corona-krise/"
                  size="large"
                  target="_blank"
                >
                  <LinkOutlined />
                  Auf zum Blog-Artikel
                </Button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <footer>
        <div className="extra-margins-sides">
          <img
            id="bottomBg"
            className="noselect"
            src="/graphics/landing_page_bottom_bg.png"
          />
          <div className="pageWrapper">
            <Link href="/imprint">
              <a>Impressum</a>
            </Link>
            <Link href="/privacy-policy">
              <a>Datenschutz</a>
            </Link>
          </div>
        </div>
      </footer>
      <style jsx>{`
        #page {
          display: flex;
          flex-direction: column;
          min-height: 100%;
          padding-bottom: 8rem;
          position: relative;
        }
        #topImageWrapper {
          position: relative;
          height: 35vh;
          overflow: hidden;
        }
        #topBg {
          position: absolute;
          width: 200%;
          left: -55%;
          bottom: 0;
        }
        #logo {
          position: absolute;
          width: 60%;
          bottom: 8vh;
          left: 50%;
          transform: translateX(-49%);
        }
        .pageWrapper {
          padding-top: 1rem;
          display: flex;
          justify-content: center;
          min-height: 100%;
        }
        #contentWrapper {
          display: flex;
          flex-direction: column;
          align-content: middle;
          width: 70%;
        }
        * > :global(button) {
          margin-top: 0.8rem;
          height: auto;
          padding: 1rem;
          font-size: 1.3rem;
        }
        .centered {
          text-align: center;
        }
        h1.main {
          font-weight: bold;
          margin-top: 2rem;
        }
        .cardBg {
          margin-top: 2rem;
          background: lightgrey url("/graphics/card_bg.svg") no-repeat center;
          -webkit-background-size: cover;
          -moz-background-size: cover;
          -o-background-size: cover;
          background-size: cover;
        }
        footer {
          position: absolute;
          width: 100%;
          min-height: 5rem;
          bottom: 0;
        }
        footer > div {
          overflow: hidden;
          position: relative;
          min-height: 5rem;
        }
        footer > div > .pageWrapper {
          margin-top: 1.15rem;
        }
        #bottomBg {
          position: absolute;
          width: 100%;
          top: 0;
        }
        footer a {
          margin: 0 0.5rem;
        }
        footer a,
        footer a:hover,
        footer a:active {
          color: white;
          z-index: 1;
        }
        footer a:hover {
          text-decoration: underline;
        }
        footer a:active {
          text-decoration: none;
        }
        .additional-section {
          margin-top: 2rem;
          font-size: 1rem;
          padding: 1.5rem;
          color: #70009c;
          display: flex;
          flex-direction: column;
          text-align: center;
        }
        .additional-section.filled {
          background-color: #70009c;
          color: white;
        }
        .additional-section #header {
          font-weight: bold;
          font-size: 1.2rem;
          margin-bottom: 0.5rem;
        }
        .additional-section #icon {
          font-size: 3rem;
        }
        .additional-section #description {
          margin-bottom: 1rem;
        }
        .additional-section #mail {
          margin-top: 1rem;
          font: "Courier New";
          background-color: #70009c30;
          padding: 0.2rem 0.5rem;
          border-radius: 5px;
        }

        @media (min-width: 700px) {
          .additional-section {
            flex-direction: row;
            text-align: justify;
          }
          .additional-section #icon {
            padding-right: 1.5rem;
          }
        }
      `}</style>
    </div>
  );
}

export default Index;
