import MenuLayout from "../components/MenuLayout";
import Menu from "../components/Menu";
import { withLoginRedirect } from "../components/withLoginRedirect";
import { useTasks } from "../components/useTasks";
import {
  ConfiguredMap,
  MarkerWithClickHandler
} from "../components/ConfiguredMap";
import Link from "next/link";
import { PlusOutlined } from "@ant-design/icons";
import { Button } from "antd";
import { taskName } from "./tasks";
import { useRouter } from "next/router";
import { i18n } from "@lingui/core";
import { t } from "@lingui/macro";
import Head from "../components/Head";

function Radar() {
  const [tasks] = useTasks();
  const router = useRouter();

  const filters = [
    { name: "Besorgung", checked: true, onClick: () => void 0 },
    { name: "Betreuung", checked: true, onClick: () => void 0 },
    { name: "Transport", checked: true, onClick: () => void 0 },
    { name: "Sonstiges", checked: true, onClick: () => void 0 }
  ];

  return (
    <MenuLayout fullScreenPage menu={<Menu active="radar" />}>
      <Head title="Hilferadar" />
      <ConfiguredMap>
        {tasks.map(task => {
          const { creator, client, _id } = task;

          function getPosition() {
            if (client) {
              return client.address.position;
            }

            return creator.address?.position;
          }

          const position = getPosition();
          if (!position) {
            return null;
          }

          const [longitude, latitude] = position;

          return (
            <MarkerWithClickHandler
              key={_id}
              latitude={latitude}
              longitude={longitude}
              onClick={() => {
                router.push("/t/[taskid]", `/t/${_id}`);
              }}
            >
              {taskName(task)}
            </MarkerWithClickHandler>
          );
        })}
      </ConfiguredMap>

      <Link href="/request">
        <Button
          className="extra-margins-sides"
          type="primary"
          size="large"
          icon={<PlusOutlined />}
          id="add-task"
        >
          {i18n._(t`Hilfe suchen`)}
        </Button>
      </Link>
      <style jsx>{`
        :global(#add-task) {
          position: absolute;
          right: 1rem;
          bottom: 1.5rem;
        }
      `}</style>
    </MenuLayout>
  );
}

export default withLoginRedirect(Radar);
