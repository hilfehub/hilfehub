import MenuLayout from "../components/MenuLayout";
import Menu from "../components/ActionButtonMenu";
import HelpRequestForm from "../components/helprequest/HelpRequestForm";
import { PropsWithChildren } from "react";
import {
  useActionButtonContext,
  ActionButtonContextProvider
} from "../components/ActionButtonContext";
import { useRouter } from "next/router";
import { i18n } from "../components/i18n";
import { t } from "@lingui/macro";
import Head from "../components/Head";

function PageMenu({ children }: PropsWithChildren<{}>) {
  const {
    clickBack,
    clickAction,
    actionButtonDefinition,
    backButtonDefinition
  } = useActionButtonContext();

  const menu = (
    <Menu
      caption={actionButtonDefinition?.caption ?? "Weiter"}
      disabled={actionButtonDefinition?.disabled}
      type={actionButtonDefinition?.type}
      onClick={clickAction}
      onClickBack={clickBack}
      backVisible={backButtonDefinition?.type !== "invisible"}
    />
  );
  return <MenuLayout menu={menu}>{children}</MenuLayout>;
}

export default function Request() {
  const router = useRouter();

  return (
    <ActionButtonContextProvider>
      <Head title="Hilfe anfragen" />
      <PageMenu>
        <HelpRequestForm
          header={i18n._(t`Hilfe anfragen`)}
          onExit={router.back}
          onDone={() => {
            router.push("/tasks");
          }}
        />
      </PageMenu>
    </ActionButtonContextProvider>
  );
}
