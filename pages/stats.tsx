import useSWR from "swr";
import { StatisticRecord } from "../backend";
import { Row, Col, Statistic } from "antd";
import { LegalLayout } from "../components/LegalLayout";
import Head from "../components/Head";

function useStatistics() {
  const { data } = useSWR<StatisticRecord>(
    "/api/statistics",
    k => fetch(k).then(r => r.json()),
    {
      refreshInterval: 5000
    }
  );
  return data;
}

function Stats() {
  const stats = useStatistics();

  if (!stats) {
    return null;
  }

  return (
    <LegalLayout>
      <Head title="Statistiken" />
      <Row gutter={16}>
        <Col span={4}>
          <Statistic title="Users" value={stats.users} />
        </Col>
        <Col span={4}>
          <Statistic title="Tasks Created" value={stats.tasksCreated} />
        </Col>
        <Col span={4}>
          <Statistic title="Tasks Completed" value={stats.tasksCompleted} />
        </Col>
      </Row>
    </LegalLayout>
  );
}

export default Stats;
