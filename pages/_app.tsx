import "../style/style.css";
import "../style/custom_antd.less";
import { AppProps } from "next/app";
import { TokenProvider } from "../components/TokenContext";
import { withErrorBoundary } from "../components/withErrorBoundary";
import { I18nProvider } from "../components/i18n";
import Head from "../components/Head";

// This default export is required in a new `pages/_app.js` file.
function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <I18nProvider>
        <TokenProvider>
          <Component {...pageProps} />
        </TokenProvider>
      </I18nProvider>
      <style global jsx>{`
        div#__next {
          height: 100%;
        }
      `}</style>
    </>
  );
}

export default withErrorBoundary(MyApp);
