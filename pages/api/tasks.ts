import { RESTHandler } from "../../backend";
import { authenticate, useUserID } from "../../backend";
import { NextApiResponse } from "next";
import { Tasks } from "../../backend";
import { Task } from "../../types";

export type CreateTaskPayload = Omit<Tasks.Create.Payload, "creatorId">;

interface CreateTaskResponse {
  _id: string;
}

export default RESTHandler(authenticate)({
  async post(req, res: NextApiResponse<CreateTaskResponse>) {
    const creatorId = useUserID(req);
    const payload = req.body as CreateTaskPayload;
    const task = await Tasks.Create({
      ...payload,
      creatorId
    });

    const { _id } = task!;

    res.setHeader("Location", `/tasks/${_id}`);

    res.status(201).json({ _id });
  },

  async get(req, res: NextApiResponse<Task[]>) {
    const tasks = await Tasks.FindOpen();
    return res.status(200).json(tasks);
  }
});
