import { RESTHandler } from "../../../backend";
import { authenticate, useUserID } from "../../../backend";
import { Addresses } from "../../../backend";
import { NextApiResponse } from "next";
import { useObjectID, validateObjectID } from "../../../backend";
import { Address } from "../../../types";

export default RESTHandler(
  authenticate,
  validateObjectID
)({
  async get(req, res: NextApiResponse<Address>) {
    const requestingUser = useUserID(req);
    const addressId = useObjectID(req);

    const address = await Addresses.FindById(addressId, requestingUser);
    if (!address) {
      return res.status(404);
    } else {
      return res.status(200).json(address);
    }
  }
});
