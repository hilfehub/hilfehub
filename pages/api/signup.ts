import { RESTHandler } from "../../backend";
import { SignUp } from "../../backend";
import { issueJWTFor } from "../../backend";
import { NextApiResponse } from "next";
import { Users } from "../../backend";
import { RegisteredUser } from "../../types";

export interface SignupResponse {
  token: string;
  user: RegisteredUser;
}

export default RESTHandler()({
  async post(req, res: NextApiResponse<SignupResponse>) {
    const { email, password, firstname, lastname } = req.body;

    const result = await SignUp({ email, password, firstname, lastname });

    await result.cata(
      async fail => {
        switch (fail) {
          case "email_in_use":
            return res.status(409).send("email already in use" as any);
        }
      },
      async uid => {
        const user = (await Users.findById(uid)) as RegisteredUser;

        const token = issueJWTFor(uid);

        res.setHeader("Location", `/users/${uid}`);

        res.status(200).json({
          token,
          user
        });
      }
    );
  }
});
