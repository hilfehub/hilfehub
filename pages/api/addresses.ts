import { RESTHandler } from "../../backend";
import { authenticate, useUserID } from "../../backend";
import { Addresses } from "../../backend";
import { NextApiResponse } from "next";
import { Address } from "../../types";

export interface PostAddressResponse {
  _id: string;
}

export default RESTHandler(authenticate)({
  async get(req, res: NextApiResponse<Address[]>) {
    const userId = useUserID(req);
    const addresses = await Addresses.FindForUser(userId);
    return res.status(200).json(addresses);
  },

  async post(req, res: NextApiResponse<PostAddressResponse>) {
    const userId = useUserID(req);
    const payload = req.body as Addresses.Create.Payload;

    const { _id } = await Addresses.Create(payload, userId);

    res.setHeader("Location", `/addresses/${_id}`);
    res.status(201).json({ _id });
  }
});
