import { RESTHandler } from "../../backend";
import { Statistics, StatisticRecord } from "../../backend";
import { NextApiResponse } from "next";

export default RESTHandler()({
  async get(req, res: NextApiResponse<StatisticRecord>) {
    const stats = await Statistics();
    return res.status(200).json(stats);
  }
});
