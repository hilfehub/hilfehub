import { authenticate, useUserID } from "../../../backend";
import { RESTHandler } from "../../../backend";
import { NextApiResponse } from "next";
import { Tasks } from "../../../backend";
import { Task } from "../../../types";

export default RESTHandler(authenticate)({
  async get(req, res: NextApiResponse<Task[]>) {
    const byUserId = useUserID(req);
    const tasks = await Tasks.FindPicked(byUserId);
    res.status(200).json(tasks);
  }
});
