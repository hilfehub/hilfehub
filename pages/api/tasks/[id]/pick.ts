import { RESTHandler } from "../../../../backend";
import { authenticate, useUserID } from "../../../../backend";
import { validateObjectID, useObjectID } from "../../../../backend";
import { Picks } from "../../../../backend";

export default RESTHandler(
  authenticate,
  validateObjectID
)({
  async post(req, res) {
    const taskId = useObjectID(req);
    const userId = useUserID(req);

    const result = await Picks.Pick(taskId, userId);

    result.cata(
      not_found => {
        res.status(404).send("Not Found");
      },
      _id => {
        res.status(200).send("Picked");
      }
    );
  }
});
