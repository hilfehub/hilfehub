import { RESTHandler } from "../../../../backend";
import { authenticate, useUserID } from "../../../../backend";
import { validateObjectID, useObjectID } from "../../../../backend";
import { Tasks } from "../../../../backend";

export default RESTHandler(
  authenticate,
  validateObjectID
)({
  async post(req, res) {
    const userId = useUserID(req);
    const taskId = useObjectID(req);

    const result = await Tasks.Complete(taskId, userId);

    result.cata(
      err => {
        switch (err) {
          case "forbidden":
            return res.status(403).send("Forbidden");
          case "not_found":
            return res.status(404).send("Not Found");
        }
      },
      () => {
        return res.status(200).send("Completed");
      }
    );
  }
});
