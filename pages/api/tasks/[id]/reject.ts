import { RESTHandler } from "../../../../backend";
import { authenticate, useUserID } from "../../../../backend";
import { validateObjectID, useObjectID } from "../../../../backend";
import { Picks } from "../../../../backend";

export default RESTHandler(
  authenticate,
  validateObjectID
)({
  async post(req, res) {
    const taskId = useObjectID(req);
    const userId = useUserID(req);

    const result = await Picks.Reject(taskId, userId);
    switch (result) {
      case "not_found":
        return res.status(404).send("Not Found");
      case "success":
        return res.status(200).end("Rejected");
    }
  }
});
