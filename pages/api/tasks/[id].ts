import { RESTHandler } from "../../../backend";
import { authenticate, useUserID } from "../../../backend";
import { NextApiResponse } from "next";
import { Tasks } from "../../../backend";
import { validateObjectID, useObjectID } from "../../../backend";
import { Task } from "../../../types";

export default RESTHandler(
  authenticate,
  validateObjectID
)({
  async get(req, res: NextApiResponse<Task>) {
    const taskId = useObjectID(req);

    const userId = useUserID(req);
    const task = await Tasks.FindById(taskId, userId);
    if (!task) {
      return res.status(404).end();
    }

    return res.status(200).json(task);
  },

  async delete(req, res: NextApiResponse) {
    const userId = useUserID(req);
    const taskId = useObjectID(req);

    const result = await Tasks.Delete(userId, taskId);
    switch (result) {
      case "forbidden":
        return res.status(403).send("Forbidden");
      case "not_found":
        return res.status(404).send("Not Found");
      case "success":
        return res.status(200).send("Deleted");
    }
  }
});
