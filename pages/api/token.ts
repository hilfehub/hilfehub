import { RESTHandler } from "../../backend";
import { authenticate, useUserID } from "../../backend";
import { issueJWTFor } from "../../backend";
import { NextApiResponse } from "next";
import { Users } from "../../backend";
import { RegisteredUser } from "../../types";

export interface TokenResponse {
  token: string;
  user: RegisteredUser;
}

export default RESTHandler(authenticate)({
  async get(req, res: NextApiResponse<TokenResponse>) {
    const userId = useUserID(req);
    const user = (await Users.findById(userId)) as RegisteredUser;
    const token = issueJWTFor(userId);
    res.status(200).send({
      user,
      token
    });
  }
});
