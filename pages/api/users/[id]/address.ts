import { RESTHandler } from "../../../../backend";
import { authenticate, useUserID } from "../../../../backend";
import { validateObjectID, useObjectID } from "../../../../backend";
import { NextApiResponse } from "next";
import { Addresses } from "../../../../backend";
import { Users } from "../../../../backend";
import { Address } from "../../../../types";

export default RESTHandler(
  authenticate,
  validateObjectID
)({
  async put(req, res: NextApiResponse<Address>) {
    const requestingUser = useUserID(req);
    const id = useObjectID(req);

    const payload = req.body as Addresses.Create.Payload;
    const address = await Users.updateAddress(payload, id, requestingUser);
    address.cata(
      err => {
        switch (err) {
          case "forbidden":
            return res.status(403).end();
        }
      },
      a => {
        return res.status(201).json(a);
      }
    );
  }
});
