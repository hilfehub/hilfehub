import { RESTHandler } from "../../../../backend";
import { authenticate, useUserID } from "../../../../backend";
import { validateObjectID, useObjectID } from "../../../../backend";
import { Users } from "../../../../backend";

export default RESTHandler(
  authenticate,
  validateObjectID
)({
  async put(req, res) {
    const requestingUserId = useUserID(req);
    const id = useObjectID(req);

    const phoneNumber = req.body as string;

    const result = await Users.updateTelephoneNumber(
      id,
      requestingUserId,
      phoneNumber
    );
    result.cata(
      err => {
        switch (err) {
          case "forbidden":
            return res.status(403).end();
        }
      },
      () => {
        return res.status(200).end();
      }
    );
  }
});
