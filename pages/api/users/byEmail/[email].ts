import { RESTHandler } from "../../../../backend";
import { Users } from "../../../../backend";

export default RESTHandler()({
  async head(req, res) {
    const email = req.query.email as string;

    const isInUse = await Users.isEmailUsed(email);

    return res.status(isInUse ? 200 : 404);
  }
});
