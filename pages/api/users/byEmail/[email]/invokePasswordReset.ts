import validator from "validator";
import { RESTHandler } from "../../../../../backend";
import { PasswordReset } from "../../../../../backend";
import { Config } from "../../../../../backend/Config";

export default RESTHandler()({
  async post(req, res) {
    const email = req.query.email as string;

    const isValidEmail = validator.isEmail(email);
    if (!isValidEmail) {
      return res.status(400).end("please provide valid email address");
    }

    await PasswordReset.Invoke(
      email,
      token => `${Config.BASE_URL}/resetPassword?token=${token}`
    );

    res.status(200).end();
  }
});
