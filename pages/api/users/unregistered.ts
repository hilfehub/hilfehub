import { authenticate, useUserID } from "../../../backend";
import { RESTHandler } from "../../../backend";
import { Users } from "../../../backend";
import { NextApiResponse } from "next";
import { UnregisteredUser } from "../../../types";

export default RESTHandler(authenticate)({
  async get(req, res: NextApiResponse<UnregisteredUser[]>) {
    const userId = useUserID(req);
    const createdUsers = await Users.findUnregisteredCreatedBy(userId);
    return res.status(200).json(createdUsers);
  },

  async post(req, res: NextApiResponse<UnregisteredUser>) {
    const userId = useUserID(req);
    const payload = req.body as Users.Create.UnregisteredUser.Payload;
    const user = await Users.Create.UnregisteredUser(payload, userId);
    res.setHeader("Location", `/users/${user._id}`);
    res.status(201).json(user);
  }
});
