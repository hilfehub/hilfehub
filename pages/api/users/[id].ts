import { RESTHandler } from "../../../backend";
import { authenticate } from "../../../backend";

export default RESTHandler(authenticate)({
  async get(req, res) {
    return res.status(500).send("Not Implemented");
  }
});
