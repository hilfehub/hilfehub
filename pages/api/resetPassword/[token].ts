import { RESTHandler } from "../../../backend";
import { NextApiResponse } from "next";
import { PasswordReset } from "../../../backend";
import { RegisteredUser } from "../../../types";

export interface ResetPasswordResponse {
  token: string;
  user: RegisteredUser;
}

export default RESTHandler()({
  async post(req, res: NextApiResponse<ResetPasswordResponse>) {
    const token = req.query.token as string;
    const password = req.body;

    const result = await PasswordReset.Set(token, password);
    if (!result) {
      return res.status(404).end();
    }

    res.status(200).json(result);
  }
});
