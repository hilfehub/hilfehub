import { useCallback, useState } from "react";
import { useRouter } from "next/router";
import { LoginSignupPage } from "../components/LoginSignupPage";
import MenuLayout from "../components/MenuLayout";
import ActionButtonMenu from "../components/ActionButtonMenu";
import { Spin } from "antd";
import { LoadingOutlined } from "@ant-design/icons";
import {
  ActionButtonContextRef,
  ActionButtonContextProvider
} from "../components/ActionButtonContext";
import { t } from "@lingui/macro";
import { i18n } from "../components/i18n";
import Head from "../components/Head";

export type HeaderType = "volunteer" | "seeker-self" | "seeker-else" | "login";

export default function Login() {
  const router = useRouter();

  const {
    query: { action: _firstPane, goBackOnDone }
  } = router;
  const firstPane = _firstPane as "login" | "signup";

  const type = router.query.type as HeaderType | undefined;

  const handleSuccessful = useCallback(() => {
    if (goBackOnDone) {
      router.back();
    } else {
      router.push("/radar");
    }
  }, [router.push, goBackOnDone]);

  const [requestRunning, setRequestRunning] = useState(false);
  const [inputValid, setInputValid] = useState(false);
  const [
    requestContextRef,
    setRequestContextRef
  ] = useState<ActionButtonContextRef | null>(null);

  const [activePane, setActivePane] = useState<"login" | "signup">(firstPane);

  const handleAction = useCallback(() => {
    requestContextRef?.clickAction();
  }, [requestContextRef]);

  let menu = (
    <ActionButtonMenu
      caption={activePane === "login" ? "Einloggen" : "Registrieren"}
      onClick={handleAction}
    />
  );

  if (type === "volunteer") {
    menu = (
      <ActionButtonMenu
        caption={
          activePane === "login" ? "Jetzt helfen" : "Registrieren & helfen"
        }
        onClick={handleAction}
      />
    );
  } else if (type === "seeker-self" || type === "seeker-else") {
    menu = (
      <ActionButtonMenu
        caption={
          activePane === "login"
            ? "Hilfe suchen"
            : "Registrieren & Hilfe suchen"
        }
        onClick={handleAction}
      />
    );
  }

  function getHeader(userType: HeaderType) {
    switch (userType) {
      case "volunteer":
        return "Du Held!";
      case "seeker-self":
        return "Hilfe naht!";
      case "seeker-else":
        return "Du Engel!";
      default:
        return activePane === "login" ? "Anmelden" : "Registrieren";
    }
  }

  function getText(userType: HeaderType) {
    switch (userType) {
      case "volunteer":
        return "Bevor du anderen helfen kannst, müssen wir wissen, wer du bist. Damit schützen wir die Daten der Hilfesuchenden.";
      case "seeker-self":
        return "Wir nehmen unsere Aufgabe, die Vermittlung, ernst! Um sicher zu sein, dass dein Hilfegesuch ebenfalls ernst gemeint ist, musst du bei uns registriert und eingeloggt sein. Deine persönlichen Daten können nur registrierte Nutzer sehen.";
      case "seeker-else":
        return "Es ist toll, dass du anderen unter die Arme greifst! Bevor du für sie Hilfe suchen kannst, müssen wir wissen, wer du bist. So gehen wir sicher, dass die Hilfegesuche ernst gemeint sind. Deine Daten werden dem bzw. der Helfenden später nicht angezeigt.";
      case "login":
        return "";
      default:
        return "Bevor du fortfahren kannst, musst du registriert und angemeldet sein.";
    }
  }

  return (
    <ActionButtonContextProvider receiveRef={setRequestContextRef}>
      <Head title="Anmelden" />
      <MenuLayout
        menu={
          <ActionButtonMenu
            caption={
              requestRunning ? (
                <Spin
                  delay={500}
                  indicator={
                    <>
                      <LoadingOutlined />
                      &nbsp;&nbsp;
                      {activePane === "login"
                        ? i18n._(t`Anmeldung`)
                        : i18n._(t`Registrierung`)}
                      ...
                    </>
                  }
                />
              ) : (
                i18n._(t`Jetzt Helfen`)
              )
            }
            onClick={handleAction}
            disabled={!inputValid || requestRunning}
          />
        }
      >
        <div className="margins-sides">
          <LoginSignupPage
            showHeader
            firstPane={firstPane}
            onInputValid={useCallback(() => setInputValid(true), [
              setInputValid
            ])}
            onInputInvalid={useCallback(() => setInputValid(false), [
              setInputValid
            ])}
            onLoginLoading={useCallback(() => setRequestRunning(true), [
              setRequestRunning
            ])}
            onLoginComplete={useCallback(() => setRequestRunning(false), [
              setRequestRunning
            ])}
            onLoginSuccessful={handleSuccessful}
            onSignupLoading={useCallback(() => setRequestRunning(true), [
              setRequestRunning
            ])}
            onSignupComplete={useCallback(() => setRequestRunning(false), [
              setRequestRunning
            ])}
            onSignupSucessful={handleSuccessful}
            onPaneChanged={setActivePane}
          />
        </div>
      </MenuLayout>
    </ActionButtonContextProvider>
  );
}
