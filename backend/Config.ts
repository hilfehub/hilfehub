export module Config {
  export const JWT_SECRET = process.env.JWT_SECRET ?? "";
  export const MONGO_URL = process.env.MONGO_URL ?? "mongodb://localhost:27017";
  export const SMTP_CONNECTION_URL = process.env.SMTP_CONNECTION_URL ?? "";
  export const SMTP_MAIL_FROM = process.env.SMTP_MAIL_FROM ?? "";
  export const BASE_URL = process.env.BASE_URL ?? "";
  export const MAPBOXGL_TOKEN = process.env.MAPBOXGL_TOKEN ?? "";
}
