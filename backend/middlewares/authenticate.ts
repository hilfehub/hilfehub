import { Middleware } from ".";
import * as basic_auth from "basic-auth";
import { verifyJWT } from "../services/JWT";
import { UserRepo } from "../db/User";
import { BCrypt } from "../util/BCrypt";
import { NextApiRequest } from "next";

function getBearerToken(authorization: string): string | null {
  if (authorization.startsWith("Bearer ")) {
    return authorization.replace("Bearer ", "");
  }

  return null;
}

function getBasicCredentials(
  authorization: string
): { name: string; pass: string } | null {
  return basic_auth.parse(authorization) ?? null;
}

async function checkJWT(jwt: string): Promise<string | null> {
  return verifyJWT(jwt)?.uid ?? null;
}

async function checkBasic(name: string, pass: string): Promise<string | null> {
  const user = await UserRepo.findByEmail(name);
  if (!user) {
    return null;
  }

  const { _id, passwordHash } = user;
  if (!passwordHash) {
    return null;
  }

  const isValid = await BCrypt.verify(pass, passwordHash);
  return isValid ? _id : null;
}

const reqs = new Map<NextApiRequest, string>();

export function useUserID(req: NextApiRequest) {
  const uid = reqs.get(req);

  if (!uid) {
    throw new Error("useUserID can only be used after authorization.");
  }

  return uid;
}

export const authenticate: Middleware = next => async (req, res) => {
  function forbid() {
    res.status(401).end("Please Authenticate.");
  }

  async function grant(userId: string) {
    reqs.set(req, userId);
    await next(req, res);
    reqs.delete(req);
  }

  const { authorization } = req.headers;
  if (!authorization) {
    return forbid();
  }

  let userId: string | null = null;

  const jwt = getBearerToken(authorization);
  if (!!jwt) {
    userId = await checkJWT(jwt);
  }

  const basic = getBasicCredentials(authorization);
  if (!!basic) {
    userId = await checkBasic(basic.name, basic.pass);
  }

  if (!!userId) {
    await grant(userId);
  } else {
    forbid();
  }
};
