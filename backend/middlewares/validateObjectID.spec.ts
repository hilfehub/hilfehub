import { isValidObjectId } from "mongoose";

describe("isValidObjectId", () => {
  test.each([
    ["", false],
    ["45cbc4a0e4123f6920000002", true],
    ["000000000000000000000000", true]
  ])("isValidObjectId($0) = $1", (value, expected) => {
    expect(isValidObjectId(value)).toBe(expected);
  });
});
