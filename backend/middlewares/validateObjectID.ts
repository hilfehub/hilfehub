import { Middleware } from ".";
import { isValidObjectId } from "mongoose";
import { NextApiRequest } from "next";
import { useUserID } from "./authenticate";

const map = new Map<NextApiRequest, string>();

export const validateObjectID: Middleware = next => (req, res) => {
  let id = req.query.id as string;

  if (id === "self") {
    id = useUserID(req);
  } else if (!isValidObjectId(id)) {
    return res.status(403).end("ID is not a valid ObjectID.");
  }

  map.set(req, id);
  next(req, res);
  map.delete(req);
};

export function useObjectID(req: NextApiRequest) {
  return map.get(req)!;
}
