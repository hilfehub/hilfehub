import { Handler } from "../util/handler";

export type Middleware = (next: Handler) => Handler;

export * from "./authenticate";
export * from "./validateObjectID";
