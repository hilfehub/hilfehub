import { UserRepo } from "../db/User";
import jwt from "jsonwebtoken";
import { Config } from "../Config";
import { BCrypt } from "../util/BCrypt";
import { issueJWTFor } from "./JWT";
import { EMail } from "../util/EMail";
import { Users } from "./Users";
import { RegisteredUser } from "../../types";

export module PasswordReset {
  interface TokenPayload {
    uid: string;
    phh: string;
  }

  async function createToken(user: RegisteredUser): Promise<string> {
    const passwordHash = await UserRepo.findPasswordHash(user._id);
    const phh = await BCrypt.hash(passwordHash!);

    const payload: TokenPayload = {
      uid: user._id,
      phh: phh
    };

    const token = jwt.sign(payload, Config.JWT_SECRET, { expiresIn: "2 days" });

    return token;
  }

  async function getUserIdOfToken(token: string): Promise<string | null> {
    try {
      const { phh, uid } = jwt.verify(token, Config.JWT_SECRET) as TokenPayload;

      const currentPasswordHash = await UserRepo.findPasswordHash(uid);
      if (!currentPasswordHash) {
        return null;
      }

      const phhIsValid = await BCrypt.verify(currentPasswordHash, phh);
      if (!phhIsValid) {
        return null;
      }

      return uid;
    } catch {
      return null;
    }
  }

  async function sendEmail(recipient: string, link: string) {
    await EMail.send(
      recipient,
      "Passwort zurücketzen (HilfeHub)",
      "Für diese E-Mail-Addresse wurde eine Passwort-Zurücksetzung angefragt.\n" +
        `Um ihr Passwort zurückzusetzen, klicken sie auf diesen Link: ${link}`
    );
  }

  export async function Invoke(
    email: string,
    createLink: (token: string) => string
  ) {
    const user = await UserRepo.findByEmail(email);
    if (!user) {
      return;
    }

    const token = await createToken(user);
    const link = createLink(token);
    await sendEmail(email, link);
  }

  export async function Set(
    token: string,
    password: string
  ): Promise<{ token: string; user: RegisteredUser } | null> {
    const userId = await getUserIdOfToken(token);
    if (!userId) {
      return null;
    }

    const passwordHash = await BCrypt.hash(password);
    await UserRepo.setPasswordHash(userId, passwordHash);
    const user = await Users.findById(userId);

    const authToken = issueJWTFor(userId);
    return {
      token: authToken,
      user: user as RegisteredUser
    };
  }
}
