import { AddressRepo } from "../db/Address";

export module Addresses {
  export async function Create(
    payload: Create.Payload,
    requestingUser: string
  ) {
    const address = await AddressRepo.create({
      ...payload,
      ownerId: requestingUser
    });
    return address!;
  }

  export module Create {
    export type Payload = Omit<AddressRepo.CreatePayload, "ownerId">;
  }

  export const FindForUser = AddressRepo.findForUser;

  export async function FindById(id: string, requestingUser: string) {
    const address = await AddressRepo.findById(id);
    if (!address) {
      return null;
    }

    const { ownerId } = address;
    if (ownerId !== requestingUser) {
      return null;
    }

    return address;
  }
}
