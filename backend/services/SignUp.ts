import { BCrypt } from "../util/BCrypt";
import { UserRepo } from "../db/User";
import { Left, Right, Either } from "monet";

export interface SignupData {
  email: string;
  password: string;
  firstname: string;
  lastname: string;
}

export async function SignUp({
  email,
  password,
  firstname,
  lastname
}: SignupData): Promise<Either<"email_in_use", string>> {
  const emailExists = await UserRepo.isEmailAlreadyUsed(email);
  if (emailExists) {
    return Left("email_in_use");
  }

  const passwordHash = await BCrypt.hash(password);

  const { _id } = await UserRepo.createRegisteredUser({
    email,
    passwordHash,
    firstname,
    lastname,
    isQuarantined: false
  });

  return Right(_id);
}
