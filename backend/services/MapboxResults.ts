import { cacheWrapper } from "../util/cacheWrapper";
import Axios from "axios";
import { Config } from "../Config";
import { MapboxGeocodingResult } from "../../components/useMapboxSearch";

export module MapboxResults {
  export const get = cacheWrapper(async (id: string) => {
    const result = await Axios.get<MapboxGeocodingResult>(
      `https://api.mapbox.com/geocoding/v5/mapbox.places/${id}.json`,
      {
        params: {
          access_token: Config.MAPBOXGL_TOKEN
        }
      }
    );

    return result.data.features[0];
  }, "mapboxResults");
}
