import { PickRepo } from "../db/Pick";
import { Left, Right, Either } from "monet";

export module Picks {
  export async function Pick(
    task: string,
    requestingUser: string
  ): Promise<Either<"not_found", string>> {
    const pick = await PickRepo.findCurrentPick(task);
    if (!!pick) {
      const pickedBySomeOneElse = pick.user._id !== requestingUser;
      return pickedBySomeOneElse ? Left("not_found") : Right(pick._id);
    }

    const { _id } = await PickRepo.createPick(requestingUser, task);
    return Right(_id);
  }

  export async function Reject(
    task: string,
    requestingUser: string
  ): Promise<"not_found" | "success"> {
    const pick = await PickRepo.findPick(task, requestingUser);
    if (!pick) {
      return "not_found";
    }

    const { _id, rejectDate } = pick;

    const alreadyRejected = !!rejectDate;
    if (!alreadyRejected) {
      await PickRepo.rejectPick(_id);
    }

    return "success";
  }
}
