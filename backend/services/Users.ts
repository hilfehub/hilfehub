import { UserRepo } from "../db/User";
import { Left, Right, Either } from "monet";
import { Addresses } from "./Addresses";
import { AddressRepo } from "../db/Address";
import { UnregisteredUser as UnregisteredUserType, Address } from "../../types";

export module Users {
  export async function isEmailUsed(email: string) {
    const user = UserRepo.findByEmail(email);
    return !!user;
  }

  export const findById = UserRepo.findById;

  export const findUnregisteredCreatedBy = UserRepo.findUnregisteredCreatedBy;

  export module Create {
    export async function UnregisteredUser(
      u: UnregisteredUser.Payload,
      creatorId: string
    ) {
      const { _id: addressId } = await Addresses.Create(u.address, creatorId);

      const user = await UserRepo.createUnregisteredUser({
        ...u,
        creatorId,
        address: addressId
      });
      await AddressRepo.setCreator(addressId, user._id);

      return user;
    }

    export module UnregisteredUser {
      export type Payload = Omit<
        UnregisteredUserType,
        "_id" | "registeredAt" | "state" | "creatorId"
      > & { address: Addresses.Create.Payload };
    }
  }

  export async function updateTelephoneNumber(
    userId: string,
    requestingUser: string,
    phoneNumber: string
  ): Promise<Either<"forbidden", true>> {
    if (userId !== requestingUser) {
      return Left("forbidden");
    }

    await UserRepo.setPhoneNumber(userId, phoneNumber);
    return Right(true);
  }

  export async function updateAddress(
    payload: Addresses.Create.Payload,
    userId: string,
    requestingUser: string
  ): Promise<Either<"forbidden", Address>> {
    if (userId !== requestingUser) {
      return Left("forbidden");
    }

    // TODO: check if user already's got an address

    const address = await Addresses.Create(payload, userId);
    await UserRepo.setAddress(userId, address._id);
    return Right(address);
  }
}
