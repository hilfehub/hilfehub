import { UserRepo } from "../db/User";
import { TaskRepo } from "../db/Task";

export interface StatisticRecord {
  users: number;
  tasksCreated: number;
  tasksCompleted: number;
}

export async function Statistics(): Promise<StatisticRecord> {
  return {
    users: await UserRepo.count(),
    tasksCreated: await TaskRepo.count(),
    tasksCompleted: await TaskRepo.countCompleted()
  };
}
