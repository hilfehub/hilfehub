import { TaskRepo } from "../db/Task";
import { PickRepo } from "../db/Pick";
import { Left, Either, Right } from "monet";

export module Tasks {
  export async function Create(task: TaskRepo.CreateTaskPayload) {
    return TaskRepo.create(task);
  }

  export module Create {
    export type Payload = TaskRepo.CreateTaskPayload;
  }

  export async function Delete(requestingUserId: string, taskId: string) {
    const task = await TaskRepo.findById(taskId);
    if (!task) {
      return "not_found";
    }

    const belongsToUser = task.creator._id === requestingUserId;
    if (!belongsToUser) {
      return "forbidden";
    }

    await TaskRepo.remove(taskId);

    return "success";
  }

  export async function FindPicked(byUser: string) {
    const picks = await PickRepo.findPicksOf(byUser);
    return picks.map(p => p.task);
  }

  export const FindCreated = TaskRepo.findByCreator;

  export const FindAll = TaskRepo.findAll;

  export const FindOpen = TaskRepo.findOpen;

  export async function FindById(taskId: string, requestingUser: string) {
    const task = await TaskRepo.findById(taskId);
    // TODO: check if requestingUser is allowed to see task
    return task;
  }

  export async function Complete(
    taskId: string,
    requestingUser: string
  ): Promise<Either<"not_found" | "forbidden", true>> {
    const task = await TaskRepo.findById(taskId);
    if (!task) {
      return Left("not_found");
    }

    const wasPickedByUser = task.pickedBy?._id === requestingUser;
    if (!wasPickedByUser) {
      return Left("forbidden");
    }

    await TaskRepo.complete(taskId);

    return Right(true);
  }
}
