import { Config } from "../Config";
import nodemailer from "nodemailer";

export module EMail {
  const mail = nodemailer.createTransport(Config.SMTP_CONNECTION_URL);

  export async function send(
    recipient: string,
    subject: string,
    body: string
  ): Promise<void> {
    return mail.sendMail({
      to: recipient,
      from: Config.SMTP_MAIL_FROM,
      subject,
      text: body
    });
  }
}
