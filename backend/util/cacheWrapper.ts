import { KeyValueRepo } from "../db/KeyValue";

export function cacheWrapper<T>(
  getter: (key: string) => Promise<T>,
  prefix = ""
): (key: string) => Promise<T> {
  async function readFromCache(key: string): Promise<T | undefined> {
    const cachedValue = await KeyValueRepo.get(prefix + ":" + key);
    return !!cachedValue ? JSON.parse(cachedValue) : undefined;
  }

  async function writeToCache(key: string, value: T) {
    const stringified = JSON.stringify(value);
    await KeyValueRepo.set(prefix + ":" + key, stringified);
  }

  return async function(key) {
    const fromCache = await readFromCache(key);
    if (!!fromCache) {
      return fromCache;
    }

    const fromGetter = await getter(key);
    writeToCache(key, fromGetter); // don't wait for writing to end
    return fromGetter;
  };
}
