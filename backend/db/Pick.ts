import { toTask } from "./Task";
import mongoose, { Schema } from "mongoose";
import { ObjectID } from "mongodb";
import { registerModel, withMongoose } from "./Mongoose";
import autopopulate from "mongoose-autopopulate";
import { Task, RegisteredUser } from "../../types";
import { toUser } from "./User";

export interface Pick {
  _id: string;
  task: Task;
  user: RegisteredUser;
  creationDate: number;
  rejectDate?: number;
}

interface IPickSchema {
  task: Task;
  user: RegisteredUser;
  rejectDate?: Date;
}

async function toPick(
  v: (IPickSchema & mongoose.Document) | null,
  dontFetchTask = false
): Promise<Pick | null> {
  if (!v) {
    return v;
  }

  return {
    _id: "" + v._id,
    task: dontFetchTask ? (null as any) : await toTask(v.task as any)!,
    user: (await toUser(v.user as any)) as RegisteredUser,
    creationDate: +(v._id as ObjectID).getTimestamp(),
    rejectDate: !!v.rejectDate ? +v.rejectDate : undefined
  };
}

const toPicks = (v: (IPickSchema & mongoose.Document)[]): Promise<Pick[]> =>
  Promise.all(v.map(v => toPick(v) as Promise<Pick>));

const PickSchema = new mongoose.Schema<IPickSchema>({
  task: {
    type: Schema.Types.ObjectId,
    ref: "Task",
    required: true,
    autopopulate: true
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: "User",
    required: true,
    autopopulate: true
  },
  rejectDate: {
    type: Date,
    required: false
  }
});

PickSchema.plugin(autopopulate);

const PickModel = registerModel("Pick", PickSchema);

export module PickRepo {
  export const createPick = (user: string, task: string) =>
    withMongoose(() =>
      PickModel.create({ user, task }).then(p => toPick(p) as Promise<Pick>)
    );

  export const findPickedTaskIds = () =>
    withMongoose(() =>
      PickModel.find({ rejectDate: { $not: { $exists: true } } }, ["task"], {
        autopopulate: false
      }).then(ps => ps.map(p => "" + p.task))
    );

  export const deletePicksOfTask = (taskId: string) =>
    withMongoose(() => PickModel.deleteMany({ _id: taskId }));

  export const findPicksOf = (user: string, includeRejected: boolean = false) =>
    withMongoose(async () => {
      const picks = await (includeRejected
        ? PickModel.find({ user } as any)
        : PickModel.find({
            user: user as any,
            rejectDate: { $exists: false }
          }));

      return toPicks(picks);
    });

  export const findCurrentPick = (
    taskId: string,
    omitTask = false
  ): Promise<Pick | null> =>
    withMongoose(() =>
      PickModel.findOne({
        task: taskId as any,
        rejectDate: { $exists: false }
      }).then(p => toPick(p, omitTask))
    );

  export const findPick = (taskId: string, userId: string) =>
    withMongoose(() =>
      PickModel.findOne({
        task: taskId as any,
        user: userId as any
      }).then(toPick)
    );

  export const rejectPick = (pickId: string) =>
    withMongoose(() =>
      PickModel.findByIdAndUpdate(pickId, { rejectDate: new Date() })
    );
}
