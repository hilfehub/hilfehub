import mongoose, { Schema } from "mongoose";
import { registerModel, withMongoose } from "./Mongoose";
import { toAddress } from "./Address";
import { toUser } from "./User";
import { ObjectID } from "mongodb";
import autopopulate from "mongoose-autopopulate";
import { PickRepo } from "./Pick";
import { MapboxResults } from "../services/MapboxResults";
import {
  UnregisteredUser,
  RegisteredUser,
  Task,
  CareTask,
  ErrandTask,
  PlainTask,
  TransportTask,
  BaseTask,
  Address
} from "../../types";

interface ITaskSchema {
  completed: boolean;
  client?: UnregisteredUser;
  creator: RegisteredUser;
  furtherInformation?: string;
  type: "errand" | "care" | "transport" | "plain";
  cart?: string;
  shopMapboxId?: string;
  description?: string;
  start?: Address;
  subject?: string;
  destination?: Address;
}

const TaskSchema = new mongoose.Schema<ITaskSchema>({
  destination: {
    type: Schema.Types.ObjectId,
    ref: "Address",
    required: false,
    autopopulate: true
  },
  completed: {
    type: Boolean,
    required: true,
    default: false
  },
  client: {
    type: Schema.Types.ObjectId,
    ref: "User",
    required: false,
    autopopulate: true
  },
  creator: {
    type: Schema.Types.ObjectId,
    ref: "User",
    required: true,
    autopopulate: true
  },
  furtherInformation: {
    type: String,
    required: false
  },
  type: {
    type: String,
    enum: ["errand", "care", "transport", "plain"],
    required: true
  },
  cart: {
    type: String,
    required: false
  },
  shopMapboxId: {
    type: String,
    required: false
  },
  description: {
    type: String,
    required: false
  },
  start: {
    type: Schema.Types.ObjectId,
    ref: "Address",
    required: false,
    autopopulate: true
  },
  subject: {
    type: String,
    required: false
  }
});

TaskSchema.plugin(autopopulate);

const TaskModel = registerModel<ITaskSchema>("Task", TaskSchema);

export async function toTask(
  v: (ITaskSchema & mongoose.Document) | null
): Promise<Task | null> {
  if (!v) {
    return v;
  }

  const pick = await PickRepo.findCurrentPick(v._id, true);

  const baseTask: BaseTask = {
    _id: "" + v._id,
    completed: !!v.completed,
    creator: (await toUser(v.creator as any)) as RegisteredUser,
    placedAt: +(v._id as ObjectID).getTimestamp(),
    client: !!v.client
      ? ((await toUser(v.client as any)) as UnregisteredUser)
      : undefined,
    furtherInformation: v.furtherInformation,
    pickedAt: pick?.creationDate,
    pickedBy: pick?.user
  };

  switch (v.type) {
    case "care":
      const careTask: CareTask = {
        ...baseTask,
        type: "care",
        description: v.description ?? ""
      };
      return careTask;
    case "errand":
      const errandTask: ErrandTask = {
        ...baseTask,
        type: "errand",
        cart: v.cart || "",
        shopMapboxId: v.shopMapboxId!,
        shopMapboxData: await MapboxResults.get(v.shopMapboxId!)
      };
      return errandTask;
    case "plain":
      const plainTask: PlainTask = {
        ...baseTask,
        type: "plain"
      };
      return plainTask;
    case "transport":
      const transportTask: TransportTask = {
        ...baseTask,
        type: "transport",
        start: (await toAddress(v.start as any))!,
        destination: (await toAddress(v.destination as any))!,
        subject: v.subject!
      };
      return transportTask;
  }
}

const toTasks = async (
  t: (ITaskSchema & mongoose.Document)[]
): Promise<Task[]> => Promise.all(t.map(v => toTask(v) as Promise<Task>));

export module TaskRepo {
  export interface CreateTaskPayload {
    type: Task["type"];
    creatorId: string;
    clientId?: string;
    furtherInformation?: string;

    cart?: string;
    shopMapboxId?: string;

    description?: string;

    startId?: string;
    destinationId?: string;
    subject?: string;
  }

  const createPlainTask = ({
    creatorId,
    furtherInformation,
    destinationId,
    clientId
  }: CreateTaskPayload) =>
    withMongoose(() =>
      TaskModel.create({
        creator: creatorId,
        destination: destinationId,
        client: clientId,
        furtherInformation,
        type: "plain"
      }).then(toTask)
    );

  const createErrandTask = ({
    creatorId,
    clientId,
    cart,
    shopMapboxId,
    furtherInformation
  }: CreateTaskPayload) =>
    withMongoose(() =>
      TaskModel.create({
        cart,
        shopMapboxId,
        creator: creatorId,
        client: clientId,
        furtherInformation,
        type: "errand"
      }).then(toTask)
    );

  const createCareTask = ({
    creatorId,
    description,
    clientId,
    furtherInformation
  }: CreateTaskPayload) =>
    withMongoose(() =>
      TaskModel.create({
        furtherInformation,
        creator: creatorId,
        client: clientId,
        description,
        type: "care"
      }).then(toTask)
    );

  const createTransportTask = ({
    creatorId,
    description,
    destinationId,
    clientId,
    startId,
    furtherInformation,
    subject
  }: CreateTaskPayload) =>
    withMongoose(() =>
      TaskModel.create({
        furtherInformation,
        creator: creatorId,
        destination: destinationId,
        client: clientId,
        description,
        type: "transport",
        start: startId,
        subject
      }).then(toTask)
    );

  const mapTypeToTaskCreator: Record<Task["type"], typeof createPlainTask> = {
    care: createCareTask,
    errand: createErrandTask,
    plain: createPlainTask,
    transport: createTransportTask
  };

  export const create = (p: CreateTaskPayload) => {
    const creator = mapTypeToTaskCreator[p.type];
    return creator(p);
  };

  export const findById = (id: string) =>
    withMongoose(() =>
      TaskModel.findById(id)
        .exec()
        .then(toTask)
    );

  export const findOpen = () =>
    withMongoose(async () => {
      const pickedTaskIds: string[] = await PickRepo.findPickedTaskIds();

      return TaskModel.find({
        completed: false,
        _id: { $not: { $in: pickedTaskIds } }
      }).then(toTasks);
    });

  export const findByIds = (ids: string[]) =>
    withMongoose(() =>
      TaskModel.find({
        _id: {
          $in: ids
        }
      })
        .exec()
        .then(toTasks)
    );

  export const remove = (id: string) =>
    withMongoose(async () => {
      await TaskModel.findByIdAndDelete(id)
        .exec()
        .then(toTask);

      await PickRepo.deletePicksOfTask(id);
    });

  export const findByCreator = (id: string) =>
    withMongoose(() =>
      TaskModel.find({
        creator: id as any
      })
        .exec()
        .then(toTasks)
    );

  export const findAll = () =>
    withMongoose(() =>
      TaskModel.find()
        .exec()
        .then(toTasks)
    );

  export const complete = (id: string) =>
    withMongoose(() =>
      TaskModel.findByIdAndUpdate(id, { completed: true }).then(toTask)
    );

  export const count = () => withMongoose(() => TaskModel.count({}));

  export const countCompleted = () =>
    withMongoose(() => TaskModel.count({ completed: true }));
}
