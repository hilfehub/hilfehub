import mongoose, { Schema } from "mongoose";
import { registerModel, withMongoose } from "./Mongoose";
import { MapboxResults } from "../services/MapboxResults";
import { Address } from "../../types";

const PointSchema = new mongoose.Schema({
  type: {
    type: String,
    enum: ["Point"],
    required: true
  },
  coordinates: {
    type: [Number],
    required: true
  }
});

const AddressSchema = new mongoose.Schema<Address>({
  streetAndNumber: {
    type: String,
    required: true
  },
  zipCode: {
    type: String,
    required: true
  },
  town: {
    type: String,
    required: true
  },
  suffix: {
    type: String,
    required: false
  },
  position: {
    type: PointSchema,
    required: false
  },
  name: {
    type: String,
    required: false
  },
  ownerId: {
    type: Schema.Types.ObjectId,
    ref: "User",
    required: true
  },
  mapboxId: {
    type: String,
    required: false
  }
});

AddressSchema.index({ position: "2dsphere" });

export async function toAddress(
  a: (Address & mongoose.Document) | null
): Promise<Address | null> {
  if (!a) {
    return a;
  }

  return {
    _id: "" + a._id,
    streetAndNumber: a.streetAndNumber,
    suffix: a.suffix,
    town: a.town,
    zipCode: a.zipCode,
    ownerId: a.ownerId,
    name: a.name,
    position: (a.position as any)?.coordinates,
    mapboxId: a.mapboxId,
    mapboxData: !!a.mapboxId ? await MapboxResults.get(a.mapboxId) : undefined
  };
}

function toAddresses(as: (Address & mongoose.Document)[]): Promise<Address[]> {
  return Promise.all(as.map(a => toAddress(a) as Promise<Address>));
}

export const AddressModel = registerModel("Address", AddressSchema);

export module AddressRepo {
  export type CreatePayload = Omit<Address, "_id">;

  export const create = (payload: CreatePayload) =>
    withMongoose(() =>
      AddressModel.create({
        ...payload,
        position: !!payload.position
          ? {
              type: "Point",
              coordinates: payload.position
            }
          : undefined
      }).then(u => toAddress(u)!)
    );

  export const findById = (id: string) =>
    withMongoose(() =>
      AddressModel.findById(id)
        .exec()
        .then(toAddress)
    );

  export const findForUser = (userId: string) =>
    withMongoose(() =>
      AddressModel.find({ ownerId: userId })
        .exec()
        .then(toAddresses)
    );

  export const setCreator = (addressId: string, creatorId: string) =>
    withMongoose(() =>
      AddressModel.findByIdAndUpdate(addressId, { creatorId }).then(toAddress)
    );
}
