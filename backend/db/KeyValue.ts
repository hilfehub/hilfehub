import mongoose from "mongoose";
import { registerModel, withMongoose } from "./Mongoose";

interface IKVSchema {
  _id: string;
  value: string;
}

const KeyValueSchema = new mongoose.Schema<IKVSchema>({
  _id: {
    type: String,
    required: true,
    unique: true
  },
  value: {
    type: String,
    required: true
  }
});

const KeyValueModel = registerModel("KV", KeyValueSchema);

export module KeyValueRepo {
  export const set = (key: string, value: string) =>
    withMongoose(() =>
      KeyValueModel.findByIdAndUpdate(key, { value }, { upsert: true })
    );

  export const get = (key: string) =>
    withMongoose(() => KeyValueModel.findById(key).then(res => res?.value));

  export const remove = (key: string) =>
    withMongoose(() => KeyValueModel.findByIdAndRemove(key));
}
