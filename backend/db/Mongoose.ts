import * as mongoose from "mongoose";
import { Config } from "../Config";

let alreadyConnected = false;

async function connect() {
  await mongoose.connect(Config.MONGO_URL, {
    useNewUrlParser: true,
    bufferCommands: false,
    bufferMaxEntries: 0,
    useCreateIndex: true
  });

  alreadyConnected = true;
}

export async function withMongoose<T>(cb: () => T | PromiseLike<T>) {
  if (!alreadyConnected) {
    await connect();
  }

  return await cb();
}

export function registerModel<T>(name: string, schema: mongoose.Schema<T>) {
  try {
    return mongoose.model<T & mongoose.Document>(name);
  } catch {
    return mongoose.model<T & mongoose.Document>(name, schema);
  }
}
