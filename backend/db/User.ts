import { withMongoose, registerModel } from "./Mongoose";
import mongoose, { Schema } from "mongoose";
import validator from "validator";
import { PhoneNumber } from "../../util/PhoneNumber";
import { ObjectID } from "mongodb";
import { toAddress } from "./Address";
import autopopulate from "mongoose-autopopulate";
import {
  User,
  BaseUser,
  RegisteredUser,
  UnregisteredUser,
  Address
} from "../../types";

interface IUserSchema {
  email?: string;
  passwordHash?: string;

  creatorId?: string;

  firstname: string;
  lastname: string;

  isQuarantined: boolean;
  telephoneNumber?: string;

  sex?: "male" | "female" | "diverse";
  formalAddress?: boolean;
  birthyear?: number;

  address?: Address;
}

const UserSchema = new mongoose.Schema<IUserSchema>({
  email: {
    type: String,
    required: false,
    unique: true,
    sparse: true,
    validate: (v: string) => validator.isEmail(v)
  },
  passwordHash: {
    type: String,
    required: false
  },

  firstname: {
    type: String,
    required: true,
    validate: (v: string) => !validator.isEmpty(v)
  },
  lastname: {
    type: String,
    required: true,
    validate: (v: string) => !validator.isEmpty(v)
  },

  creatorId: {
    type: Schema.Types.ObjectId,
    ref: "User",
    required: false
  },

  isQuarantined: {
    type: Boolean,
    required: true,
    default: false
  },
  telephoneNumber: {
    type: String,
    required: false,
    validate: PhoneNumber.isValid
  },
  sex: {
    type: String,
    required: false,
    enum: ["male", "female", "diverse"]
  },
  formalAddress: {
    type: Boolean,
    required: false
  },
  birthyear: {
    type: Number,
    required: false
  },
  address: {
    type: Schema.Types.ObjectId,
    ref: "Address",
    required: false,
    autopopulate: true
  }
});

UserSchema.plugin(autopopulate);

const UserModel = registerModel("User", UserSchema);

export async function toUser(
  v: (IUserSchema & mongoose.Document) | null
): Promise<User | null> {
  if (!v) {
    return v;
  }

  const baseUser: BaseUser = {
    _id: "" + v._id,
    firstname: v.firstname,
    isQuarantined: v.isQuarantined,
    lastname: v.lastname,
    registeredAt: +(v._id as ObjectID).getTimestamp(),
    birthyear: v.birthyear,
    formalAddress: v.formalAddress,
    sex: v.sex,
    telephoneNumber: v.telephoneNumber,
    address: (await toAddress(v.address as any))!
  };

  const state = !!v.email ? "registered" : "unregistered";

  switch (state) {
    case "registered":
      const registeredUser: RegisteredUser = {
        ...baseUser,
        state: "registered",
        email: v.email!.toLowerCase()
      };
      return registeredUser;
    case "unregistered":
      const unregisteredUser: UnregisteredUser = {
        ...baseUser,
        state: "unregistered",
        creatorId: v.creatorId!,
        telephoneNumber: v.telephoneNumber!,
        address: baseUser.address!
      };
      return unregisteredUser;
  }
}

export module UserRepo {
  export const findById = (id: string): Promise<User | null> =>
    withMongoose(() =>
      UserModel.findById(id)
        .exec()
        .then(toUser)
    );

  export const isEmailAlreadyUsed = (email: string) =>
    withMongoose(() => UserModel.exists({ email: email.toLowerCase() }));

  export const findByEmail = (email: string) =>
    withMongoose(
      () =>
        UserModel.findOne({
          email: email.toLowerCase()
        }).then(async u => {
          return {
            ...(await toUser(u)),
            passwordHash: u?.passwordHash
          };
        }) as Promise<RegisteredUser | null>
    );

  const createUser = <T extends UnregisteredUser | RegisteredUser>(
    u: Omit<IUserSchema, "_id" | "registeredAt">
  ) =>
    withMongoose(() => UserModel.create(u).then(u => toUser(u) as Promise<T>));

  export const createUnregisteredUser: (
    u: Omit<UnregisteredUser, "state" | "registeredAt" | "_id" | "address"> & {
      address: string;
    }
  ) => Promise<UnregisteredUser> = createUser;

  export const createRegisteredUser: (
    u: Omit<RegisteredUser, "state" | "registeredAt" | "_id">
  ) => Promise<RegisteredUser> = createUser;

  export const setPasswordHash = (uid: string, passwordHash: string) =>
    withMongoose(() =>
      UserModel.findByIdAndUpdate(uid, { passwordHash }).then(
        u => toUser(u) as Promise<RegisteredUser>
      )
    );

  export const findPasswordHash = (uid: string) =>
    withMongoose(() => UserModel.findById(uid).then(u => u?.passwordHash));

  export const setPhoneNumber = (uid: string, number: string) =>
    withMongoose(() =>
      UserModel.findByIdAndUpdate(uid, { telephoneNumber: number }).then(toUser)
    );

  export const setAddress = (uid: string, addressId: string) =>
    withMongoose(() =>
      UserModel.findByIdAndUpdate(uid, { address: addressId }).then(toUser)
    );

  export const findUnregisteredCreatedBy = (uid: string) =>
    withMongoose(() =>
      UserModel.find({ creatorId: uid }).then(us =>
        Promise.all(us.map(u => toUser(u) as Promise<UnregisteredUser>))
      )
    );

  export const count = () => withMongoose(() => UserModel.count({}));
}
