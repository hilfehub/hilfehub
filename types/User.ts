import { Address } from "./Address";

export interface BaseUser {
  _id: string;

  firstname: string;
  lastname: string;

  registeredAt: number;

  isQuarantined: boolean;
  telephoneNumber?: string;
  sex?: "male" | "female" | "diverse";
  formalAddress?: boolean;
  birthyear?: number;

  address?: Address;
}

export interface UnregisteredUser extends BaseUser {
  state: "unregistered";
  creatorId: string;
  address: Address;
  telephoneNumber: string;
}

export interface RegisteredUser extends BaseUser {
  state: "registered";
  email: string;
  passwordHash?: string;
}

export type User = UnregisteredUser | RegisteredUser;
