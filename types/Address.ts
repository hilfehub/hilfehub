import { MapboxGeocodingFeature } from "../components/useMapboxSearch";

export interface Address {
  _id: string;
  streetAndNumber: string;
  zipCode: string;
  town: string;
  suffix: string;
  position?: [number, number];
  name?: string;
  ownerId: string;
  mapboxId?: string;
  mapboxData?: MapboxGeocodingFeature;
}
