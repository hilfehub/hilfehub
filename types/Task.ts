import { MapboxGeocodingFeature } from "../components/useMapboxSearch";
import { RegisteredUser, UnregisteredUser } from "./User";
import { Address } from "./Address";

export interface BaseTask {
  _id: string;
  client?: UnregisteredUser;
  creator: RegisteredUser;
  placedAt: number;
  furtherInformation?: string;
  completed: boolean;
  pickedBy?: RegisteredUser;
  pickedAt?: number;
}

export interface PlainTask extends BaseTask {
  type: "plain";
}

export interface ErrandTask extends BaseTask {
  type: "errand";
  cart: string;
  shopMapboxId: string;
  shopMapboxData: MapboxGeocodingFeature;
}

export interface CareTask extends BaseTask {
  type: "care";
  description: string;
}

export interface TransportTask extends BaseTask {
  type: "transport";
  start: Address;
  destination: Address;
  subject: string;
}

export type Task = PlainTask | ErrandTask | CareTask | TransportTask;
