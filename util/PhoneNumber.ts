import { PhoneNumberUtil, PhoneNumberFormat } from "google-libphonenumber";

const util = PhoneNumberUtil.getInstance();

export module PhoneNumber {
  export function isValid(v: string): boolean {
    try {
      const n = util.parseAndKeepRawInput(v);
      return util.isValidNumber(n);
    } catch {
      return false;
    }
  }

  export function normalize(v: string): string {
    const n = util.parseAndKeepRawInput(v);
    return util.format(n, PhoneNumberFormat.INTERNATIONAL);
  }
}
