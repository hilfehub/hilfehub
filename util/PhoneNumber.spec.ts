import { PhoneNumber } from "./PhoneNumber";

describe("PhoneNumber", () => {
  describe("#isValid", () => {
    test.each`
      number                | expected
      ${"+49 1573 1829390"} | ${true}
      ${"+49 9948-545037"}  | ${true}
    `("isValidPhoneNumber($number) = $expected", ({ number, expected }) => {
      expect(PhoneNumber.isValid(number)).toBe(expected);
    });

    describe("needs country calling code", () => {
      test.each`
        number             | expected
        ${"01573 1829390"} | ${false}
        ${"09948-545037"}  | ${false}
      `("isValidPhoneNumber($number) = $expected", ({ number, expected }) => {
        expect(PhoneNumber.isValid(number)).toBe(expected);
      });
    });
  });

  describe("#normalize", () => {
    test.each`
      number                | expected
      ${"+49 1573 1829390"} | ${"+49 1573 1829390"}
      ${"+49 9948-545037"}  | ${"+49 9948 545037"}
      ${"+49 9948-545037"}  | ${"+49 9948 545037"}
    `("normalize($number) = $expected", ({ number, expected }) => {
      expect(PhoneNumber.normalize(number)).toBe(expected);
    });
  });
});
