export const singleton = <T>(factory: () => T) => {
  let v: T | null = null;
  return () => {
    if (!v) {
      v = factory();
    }
    return v;
  };
};
