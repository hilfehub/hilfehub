const webpack = require("webpack");
const withCSS = require("@zeit/next-css");
const withSass = require("@zeit/next-sass");
const withLess = require("@zeit/next-less");
const withSourceMaps = require("@zeit/next-source-maps")();
const SentryCliPlugin = require("@sentry/webpack-plugin");
require("dotenv").config();

// fix: prevents error when .less files are required by node
if (typeof require !== "undefined") {
  require.extensions[".less"] = file => {};
}

module.exports = withSourceMaps(
  withLess(
    withSass(
      withCSS({
        lessLoaderOptions: {
          javascriptEnabled: true
        },
        env: {
          MAPBOXGL_TOKEN: process.env.MAPBOXGL_TOKEN,
          MONGO_URL: process.env.MONGO_URL,
          JWT_SECRET: process.env.JWT_SECRET,
          SMTP_CONNECTION_URL: process.env.SMTP_CONNECTION_URL,
          SMTP_MAIL_FROM: process.env.SMTP_MAIL_FROM,
          BASE_URL: process.env.BASE_URL,
          IPINFO_TOKEN: process.env.IPINFO_TOKEN,
          SENTRY_DSN: process.env.SENTRY_DSN
        },
        webpack(config, { isServer, buildId }) {
          const sentryRealease = "r-" + buildId;

          const isProd = process.env.NODE_ENV === "production";
          if (isProd) {
            process.env.SENTRY_NO_PROGRESS_BAR = 1;
            config.plugins.push(
              new SentryCliPlugin({
                release: sentryRealease,
                include: `./.next/static/${buildId}/pages`
              })
            );
          }

          config.plugins.push(
            new webpack.DefinePlugin({
              "process.env.SENTRY_RELEASE": JSON.stringify(sentryRealease)
            })
          );

          if (isServer) {
            config.resolve.alias["@sentry/browser"] = "@sentry/node";
          }

          return config;
        }
      })
    )
  )
);
